<?php

// NOTE: see "/inc/classes/segmented-walker.php" for more info on how this works
function additional_active_item_classes( $classes = array(), $menu_item = false ) {

    global $wp_query;
    
    // bug in wordpress that sets blog as page parent for custom post types
    // if (
    //     $menu_item->title == 'Blog' && 
    //     (
    //         is_post_type_archive( 'case_study' )   || is_singular( 'case_study' )   ||
    //         is_post_type_archive( 'company_news' ) || is_singular( 'company_news' ) ||
    //         is_post_type_archive( 'article' )      || is_singular( 'article' )
    //     )
    // ) {
    //     $classes = array_diff( $classes, array( 'current_page_parent' ) );
    // }
    
    // for custom post archives and single posts
    // if (
    //     ( $menu_item->title == 'Resources' || $menu_item->title == 'Case Studies' ) && ( is_post_type_archive( 'case_study' )   || is_singular( 'case_study' ) )   ||
    //     ( $menu_item->title == 'Resources' || $menu_item->title == 'Company News' ) && ( is_post_type_archive( 'company_news' ) || is_singular( 'company_news' ) ) ||
    //     ( $menu_item->title == 'Resources' || $menu_item->title == 'Articles' )     && ( is_post_type_archive( 'article' )      || is_singular( 'article' ) )
    // ) {
    //     $classes[] = 'current-page-ancestor';
    //     $classes[] = 'current_page_parent';
    // }
    
    return $classes;

}
// add_filter( 'nav_menu_css_class', 'additional_active_item_classes', 1, 2 );

add_filter('acf/load_value/name=flexible_content', 'add_starting_repeater', 10, 3);
function add_starting_repeater($value, $post_id, $field) {
    if ($value !== NULL) {
      // $value will only be NULL on a new post
      return $value;
    }
    
    // add default layouts
    $value = array(
      array(
        'acf_fc_layout' => 'introduction_textimage'
      ),
      array(
        'acf_fc_layout' => 'image_and_text_section'
      ),
      array(
        'acf_fc_layout' => 'cta_section'
      )
    );
    return $value;
}

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


