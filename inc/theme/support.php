<?php
/**
 * Set Up theme support and functionality
 *
 * @return void
 */
function fx_setup() {

    // Add our own editor style
    add_editor_style();
    add_theme_support( 'title-tag' );

    // Post Formats
    // add_theme_support( 'post-formats', array('gallery', 'image', 'video', 'audio') );

    // Theme Images
    add_theme_support( 'post-thumbnails' );
    // add_image_size( 'page-header', 1600, 396, true ); // true hard crops, false proportional

    // HTML5 Support
    add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
}
add_action( 'after_setup_theme', 'fx_setup' );

/**
 * Register functionality, initilize plugin functionality
 *
 * @return void
 */
function fx_init() {
    // Register Menu
    register_nav_menus(
        array(
            'footer_menu'  => 'Navigation items for footer navigation.',
            'main_menu' => 'Navigation items for the main menu.'
        )
    );
}
add_action( 'init', 'fx_init' );

/**
 *  Register sidebars and widgets
 *
 *  @return  void
 */
function fx_widget_init() {
    // Sidebar
    register_sidebar(
        array(
            'name'          => __( 'Main Sidebar Widgets' ),
            'id'            => 'sidebar',
            'description'   => __( 'Widgets for the default sidebar' ),
            'before_title'  => '<h3>',
            'after_title'   => '</h3>',
            'before_widget' => '<div class="widget %2$s" id="%1$s" >',
            'after_widget'  => '</div>',
        )
    );
}
add_action( 'widgets_init', 'fx_widget_init' );

/**
 * Add "Styles" drop-down
 *
 * @param  array $buttons current buttons to be setup.
 * @return array
 */
function fx_mce_editor_buttons( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}
add_filter( 'mce_buttons_2', 'fx_mce_editor_buttons' );

/**
 * Add styles/classes to the "Styles" drop-down
 *
 * @param  array $settings Settings array for TinyMCE.
 * @return array
 */
function fx_mce_before_init( $settings ) {
    $style_formats = array(
        array(
            'title' => 'Primary Button',
            'selector' => 'a',
            'classes' => 'btn'
        ),
        array(
            'title' => 'Secondary Button',
            'selector' => 'a',
            'classes' => 'btn-secondary'
        ),
        array(
            'title' => 'Tertiary Button Dark',
            'selector' => 'a',
            'classes' => 'btn-tertiary-dark'
        ),
        array(
            'title' => 'Tertiary Button Light',
            'selector' => 'a',
            'classes' => 'btn-tertiary-light'
        ),
        array(
            'title' => 'Testimony Author',
            'selector' => 'p',
            'classes' => 'testimony-author'
        ),
        array(
            'title' => 'Testimony Job Position',
            'selector' => 'p',
            'classes' => 'testimony-job-position'
        ),
        array(
            'title' => 'Testimony Location',
            'selector' => 'p',
            'classes' => 'testimony-location'
        )

        /*
        Examples for adding styles
        array(
            'title' => 'Call Out Text',
            'selector' => 'p',
            'classes' => 'callout'
        )
        ,array(
            'title' => 'Warning Box',
            'block' => 'div',
            'classes' => 'warning box',
            'wrapper' => true
        )
        ,array(
            'title' => 'Red Uppercase Text',
            'inline' => 'span',
            'styles' => array(
                'color' => '#ff0000',
                'fontWeight' => 'bold',
                'textTransform' => 'uppercase'
            )
        )
        */
    );

    $settings['style_formats'] = wp_json_encode( $style_formats );

    return $settings;
}
add_filter( 'tiny_mce_before_init', 'fx_mce_before_init' );

function change_breadcrumb_single_wrapper( $element ) {
    return 'li';
}
add_filter( 'wpseo_breadcrumb_single_link_wrapper', 'change_breadcrumb_single_wrapper' );

function remove_breadcrumb_single_link_sep( $output, $link ) {
    return str_replace( '|', '', $output );
}
add_filter( 'wpseo_breadcrumb_single_link_with_sep', 'remove_breadcrumb_single_link_sep', 10, 2 );


/**
 * Create an SEO-friendly image tag based on a supplied arguments
 *
 * @param   mixed   $image      Image ID (integer/string) or image URL (string).
 * @param   mixed   $classes    Either string or array of classes.
 * @param   boolean $lazyload   True, to set up tag for lazyloading.
 * @param   string  $size       Image size.
 * @param   array   $atts       Additional attributes to add to tag.
 *
 * @return  string              Image tag
 */
function fx_get_image_tag( $image, $classes = '', $lazyload = false, $size = 'large', $atts = [] ) {
    $image_id = null;

    // determine if image ID or URL
    if ( is_numeric( $image ) ) {
        $image_id = intval( $image );
    } else {

        // try to find ID based on URL
        if ( empty( $image_id = attachment_url_to_postid( $image ) ) ) {
            return false;
        }
    }

    // set image to lazy load?
    $lazyload = boolval( $lazyload );

    // if classes weren't passed as string, try to form string
    if ( is_array( $classes ) ) {
        $classes = implode( ' ', $classes );
    }

    // need to add lazyload class?
    if ( $lazyload ) {
        if ( strpos( $classes, 'lazyload' ) === false ) {
            $classes = sprintf( '%s lazyload', $classes );
        }

		// check if lazyload class was added, but forgot to set lazyload to true
    } else {
        if ( strpos( $classes, 'lazyload' ) !== false ) {
            $lazyload = true;
        }
    }

    // if atts wasn't passed as array, use empty array
    if ( ! is_array( $atts ) ) {
        $atts = [];
    }

    // combine all tag attributes
    $atts = array_merge( [ 'class' => $classes ], $atts );
    $atts = array_filter( $atts );

    // use WP's native image tag functionality
    $tag = wp_get_attachment_image( $image_id, $size, false, $atts );

    // set image to lazyload?
    if ( $lazyload ) {
        $tag = str_replace( [ 'src="', 'srcset="' ], [ 'data-src="', 'data-srcset="' ], $tag );
    }

    return $tag;
}

/**
 * Get the raw phone number (eg: "5555555555")
 *
 * @return string
 */
function get_raw_phone_number() {
    return get_option( 'fx_phone_number' );
}

/**
 * Get the formatted phone number based off the format selected in the WP Admin (eg: (555) 555-5555
 *
 * @return string
 */
function get_formatted_phone_number() {
    $phone = get_raw_phone_number();
    if ( strlen( $phone ) === 10 ) {
        $phone  = str_split( $phone );
        $format = get_option( 'fx_phone_format' );
        if ( is_string( $format ) ) {
            $format = str_split( $format );
            $result = '';
            foreach ( $format as $char ) {
                if ( 'X' === $char ) {
                    $char = array_shift( $phone );
                }
                $result .= $char;
            }
            $phone = $result;
        }
    }
    return $phone;
}

/**
 * Provides the value for the href attribute of an "a"-tag (eg: "tel:5555555555")
 *
 * @return string
 */
function get_callable_phone_link() {
    return 'tel:' . get_raw_phone_number();
}

/**
 * If the user has a vanity number, this provides that number, otherwise is falls back to the formatted phone number
 * (eg: 555-CALL-MEE)
 *
 * @return string
 */
function get_phone_display() {
    $display = get_option( 'fx_displayable_phone_number' );
    if ( empty( $display ) ) {
        $display = get_formatted_phone_number();
    }
    if ( ! is_string( $display ) ) {
        $display = '';
    }
    return $display;
}

add_action( 'admin_init', 'add_admin_fields' );
function add_admin_fields() {
    add_settings_section( 'fx_phone_number', 'Phone Number', '__return_true', 'general' );
    add_settings_field(
        'fx_phone_number',
        'Raw Phone Number',
        'phone_number_admin_field',
        'general',
        'fx_phone_number',
        [ 'label_for' => 'fx_phone_number' ],
	);
    add_settings_field(
        'fx_displayable_phone_number',
        'Displayable Phone Number',
        'phone_display_admin_field',
        'general',
        'fx_phone_number',
        [ 'label_for' => 'fx_displayable_phone_number' ],
	);
    add_settings_field(
        'fx_phone_format',
        'Phone Format',
        'phone_format_admin_field',
        'general',
        'fx_phone_number',
        [ 'label_for' => 'fx_phone_format' ],
    );
    register_setting( 'general', 'fx_phone_number' );
    register_setting( 'general', 'fx_displayable_phone_number' );
    register_setting( 'general', 'fx_phone_format' );
}

/**
 * @param array $args Arguments supplied in add_settings_field().
 */
function phone_number_admin_field( $args ) {
    $name  = $args['label_for'];
    $value = get_option( $name );
    ?>
    <input type="tel" id="<?php echo $name; ?>" name="<?php echo $name; ?>" value="<?php echo $value; ?>" />
    <?php
}

/**
 * @param array $args Arguments supplied in add_settings_field().
 */
function phone_display_admin_field( $args ) {
    $name  = $args['label_for'];
    $value = get_option( $name );
    ?>
    <input type="text" id="<?php echo $name; ?>" name="<?php echo $name; ?>" value="<?php echo $value; ?>" />
    <?php
}

/**
 * If new formats are desired, this is the place to add them!
 *
 * @param array $args Arguments supplied in add_settings_field().
 */
function phone_format_admin_field( $args ) {
    $name  = $args['label_for'];
    $value = get_option( $name );
    ?>
    <?php //phpcs:ignore ?>
    <input type="radio" id="<?php echo $name; ?>-parens" name="<?php echo $name; ?>"<?php if ( '(XXX) XXX-XXXX' === $value ) { echo ' checked="checked"'; } ?> value="(XXX) XXX-XXXX" />
    <label for="<?php echo $name; ?>-parens">(XXX) XXX-XXXX</label>
    <br/>
    <?php //phpcs:ignore ?>
    <input type="radio" id="<?php echo $name; ?>-hyphens" name="<?php echo $name; ?>"<?php if ( 'XXX-XXX-XXXX' === $value ) { echo ' checked="checked"'; } ?> value="XXX-XXX-XXXX" />
    <label for="<?php echo $name; ?>-hyphens">XXX-XXX-XXXX</label>
    <?php
}

require_once 'Mobile_Detect.php';

/* REMOVE FOOTER STYLES OF PLUGIN */
add_filter( 'searchwp_live_search_base_styles', '__return_false' );

/* remove 'type' on style and script */
add_action(
    'after_setup_theme',
    function() {
        add_theme_support( 'html5', [ 'script', 'style' ] );
    }, 99
);
