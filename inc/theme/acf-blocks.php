<?php
/**
 * Register FX blocks
 *
 * fx_register_block() is, at its core, a wrapper function for acf_register_block_type with additional parameters for
 * our supporting functionality
 *
 * @see Guru card: https://app.getguru.com/card/Tn9zzk8c/FX-ACF-Blocks
 * @see more info for acf_register_block_type(): https://www.advancedcustomfields.com/resources/acf_register_block_type/
 *
 * Below is a reference for the parameters you can pass to fx_register_block(). You can also pass any setting from
 * acf_register_block_type() to fx_register_block().
 *
 * Required arguments: "name", "title", and "template"
 *
 */
$reference_settings = [
    // required
    'name'                  => '', // (string) unique name to identify block (no spaces)
    'title'                 => '', // (string) display title for block
    'template'              => '', // (string) relative path of the template for block (e.g "/block-templates/innerpage/template.php")

    // optional
    'css'                   => '', // (string) block-specific stylesheet. Assumed to be in /themes/fx/assets/css, so use relative path (e.g. "homepage/homepage-block.css")
    'css_deps'              => [], // (array|string) CSS dependency handles. These will be loaded before block's stylesheet. Dependencies must already be registered
    'js'                    => '', // (string) block-specific script. Assumed to be in /themes/fx/assets/js, so use relative path (e.g. "homepage/homepage-block.js")
    'js_deps'               => [], // (array|string) JS dependency handles. These will be loaded before block's script. Dependencies must already be registered
    'description'           => '', // (string) short, useful description of block to indicate block's purpose
    'category'              => '', // (string) category for where block appears in Block Library
    'icon'                  => '', // (array|string) can be a dashicon or SVG image used to identify the block
    'keywords'              => '', // (array) terms to help find block in block editor
    'post_types'            => [], // (array) if declared, will restrict block to being available for only specified post types. Default is "page"
    'exclude_post_types'    => [], // (array) post types that block should NOT appear for
    'mode'                  => '', // (string) display mode for block when you add block in Block Editor
    'supports'              => '', // (associative array) features to support. See https://developer.wordpress.org/block-editor/developers/block-api/block-supports/
];
/**
 * General blocks
 *
 * These blocks are intended to be used anywhere, including the homepage and innerpage.
 *
 * Block template path: /themes/fx/block-templates/general
 * Stylesheet path:     /themes/fx/assets/css/general
 * Script path:         /themes/fx/assets/js/general
 *
 */
/**
 * Create a "FX General Blocks" category in the block editor. Use "fx-general-blocks" as your "category" value in
 * fx_register_block()
 *
 */
fx_add_block_category( 'FX General Blocks', 'fx-general-blocks' );
/**
 * Plan WYSIWYG block for general usage
 *
 */
fx_register_block(
    [
        'name'          => 'wysiwyg',
        'title'         => 'WYSIWYG',
        'template'      => 'general/wysiwyg.php',
        'description'   => 'A basic "What you see is what you get" editor.',
        'post_types'    => [],
    ]
);

fx_register_block(
    [
        'name'          => 'newsletter',
        'title'         => 'Newsletter',
        'template'      => 'general/newsletter.php',
        'description'   => 'Block for the newsletter.',
        'post_types'    => [],
    ]
);

fx_register_block(
    [
        'name'          => 'card-service',
        'title'         => 'Card Service',
        'template'      => 'general/card-services-section.php',
        'description'   => 'Block for the card service.',
        'post_types'    => [],
    ]
);

fx_register_block(
    [
        'name'          => 'client-testimonial',
        'title'         => 'Client Testimonial',
        'template'      => 'general/client-testimonial.php',
        'description'   => 'Block for the client testimonial.',
        'post_types'    => [],
    ]
);

fx_register_block(
    [
        'name'          => 'cta',
        'title'         => 'Call To Action',
        'template'      => 'general/cta-section.php',
        'description'   => 'Block for the call to action.',
        'post_types'    => [],
    ]
);

fx_register_block(
    [
        'name'          => 'image-text',
        'title'         => 'Image and Text',
        'template'      => 'general/image-and-text.php',
        'description'   => 'Block for the image and text.',
        'post_types'    => [],
    ]
);

fx_register_block(
    [
        'name'          => 'image-buttons',
        'title'         => 'Image Buttons',
        'template'      => 'general/image-buttons.php',
        'description'   => 'Block for the image buttons.',
        'post_types'    => [],
    ]
);

fx_register_block(
    [
        'name'          => 'image-fade',
        'title'         => 'Image with Fade',
        'template'      => 'general/image-with-fade.php',
        'description'   => 'Block for the image with fade.',
        'post_types'    => [],
    ]
);

fx_register_block(
    [
        'name'          => 'introduction',
        'title'         => 'Introduction Text Image',
        'template'      => 'general/introduction.php',
        'description'   => 'Block for the introduction text image',
        'post_types'    => [],
    ]
);

fx_register_block(
    [
        'name'          => 'team',
        'title'         => 'Team',
        'template'      => 'general/team-section.php',
        'description'   => 'Block for the team',
        'post_types'    => [],
    ]
);

fx_register_block(
    [
        'name'          => 'toc-bar',
        'title'         => 'TOC Bar',
        'template'      => 'general/toc-bar.php',
        'description'   => 'Block for the toc bar',
        'post_types'    => [],
    ]
);

/**
 * To avoid issues with CF7 assets, we're creating our own CF7 block. You shouldn't need to touch this section.
 *
 */
/*$cf7_settings = [
    'name'          => 'cf7-block',
    'title'         => 'CF7 Block',
    'template'      => 'general/cf7-block.php',
    'description'   => 'Adds a CF7 block to page',
    'css_deps'      => [ 'fx_choices_custom', 'contact-form-7' ],
    'js_deps'       => [ 'contact-form-7', 'wpcf7-recaptcha', 'google-recaptcha' ],
    'keywords'      => [ 'cf7', 'contact', 'form' ],
    'mode'          => 'edit',
    'post_types'    => [], // all post types
];
$cf7_icon = WP_PLUGIN_DIR . '/contact-form-7/assets/icon.svg';
if( file_exists( $cf7_icon ) ) {
    $cf7_settings['icon'] = file_get_contents( $cf7_icon );
}
fx_register_block( $cf7_settings );*/
/**
 * Homepage blocks
 *
 * These blocks are intended to be used ONLY on the homepage.
 *
 * Block template path: /themes/fx/block-templates/homepage
 * Stylesheet path:     /themes/fx/assets/css/homepage
 * Script path:         /themes/fx/assets/js/homepage
 *
 */
/**
 * Create a "FX Homepage Blocks" category in the block editor. Use "fx-homepage-blocks" as your "category" value in
 * fx_register_block()
 *
 */
fx_add_block_category( 'FX Homepage Blocks', 'fx-homepage-blocks' );
/**
 * This is the main homepage "outer block." All other homepage blocks should be added within this block in the Block
 * Editor and in block-templates/homepage/homepage-block.php
 *
 */
fx_register_block(
    [
        'name'          => 'homepage-block',
        'title'         => 'Homepage',
        'template'      => 'homepage/homepage-block.php',
        'description'   => 'The main content block for the homepage',
        'mode'          => 'preview',
        'supports'      => [ 'jsx' => true ], // enables support for inner blocks
        'category'      => 'fx-homepage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'homepage-banner',
        'title'         => 'Homepage - Banner',
        'template'      => 'homepage/homepage-banner.php',
        'description'   => 'Block for the homepage banner.',
        'category'      => 'fx-homepage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'homepage-icontext',
        'title'         => 'Homepage - Icon + Text',
        'template'      => 'homepage/homepage-icontext.php',
        'description'   => 'Block for the homepage icon text.',
        'category'      => 'fx-homepage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'homepage-slider',
        'title'         => 'Homepage - Slider',
        'template'      => 'homepage/homepage-slider.php',
        'description'   => 'Block for the homepage slider.',
        'category'      => 'fx-homepage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'homepage-testimonial',
        'title'         => 'Homepage - Testimonial',
        'template'      => 'homepage/homepage-testimonial.php',
        'description'   => 'Block for the homepage testimonial.',
        'category'      => 'fx-homepage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'homepage-sponsors',
        'title'         => 'Homepage - Sponsors',
        'template'      => 'homepage/homepage-sponsors.php',
        'description'   => 'Block for the homepage sponsors.',
        'category'      => 'fx-homepage-blocks',
    ]
);

fx_register_block(
    [
        'name'          => 'homepage-about',
        'title'         => 'Homepage - About',
        'template'      => 'homepage/homepage-about.php',
        'description'   => 'Block for the homepage about.',
        'category'      => 'fx-homepage-blocks',
    ]
);

/**
 * Innerpage blocks
 *
 * These blocks are intended to be used ONLY on innerpages
 *
 * Block template path: /themes/fx/block-templates/innerpage
 * Stylesheet path:     /themes/fx/assets/css/innerpage
 * Script path:         /themes/fx/assets/js/innerpage
 *
 */
fx_register_block(
    [
        'name'          => 'innerpage-contact',
        'title'         => 'Innerpage - Contact Us',
        'template'      => 'innerpage/contact.php',
        'description'   => 'Block for the innerpages contacts us',
        'css'           => 'innerpage/contact.css',
        'category'      => 'fx-innerpage-blocks',
    ]
);
/**
 * Create a "FX Innerpage Blocks" category in the block editor. Use "fx-innerpage-blocks" as your "category" value in
 * fx_register_block()
 *
 */
fx_add_block_category( 'FX Innerpage Blocks', 'fx-innerpage-blocks' );
