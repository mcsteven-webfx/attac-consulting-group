<?php
/**
 * Output style to change logo on login
 *
 * @return void
 */
function fx_login_logo() {
    ?>
    <style type="text/css">
        h1 a {
            background-image:url('<?php echo get_template_directory_uri(); ?>/assets/img/admin-logo.png') !important;
            background-size: 125px 40px !important;
            height: 40px !important;
            width: 125px !important;
            margin-bottom: 0 !important;
            padding-bottom: 0 !important;
        }
        .login form { margin-top: 25px !important; }

        #nav {
            float: right !important;
            width: 50%;
            padding: 0 !important;
            text-align: right !important;
        }

        #backtoblog {
            float: left !important;
            width: 50%;
            padding: 0 !important;
            margin-top: 24px;
        }
    </style>
    <?php
}
add_action( 'login_head', 'fx_login_logo' );

/**
 * Removes Items from the sidebar that aren't need
 *
 * @return void
 */
function fx_remove_admin_menu_items() {
    global $menu;

    // array of item names to remove
    $remove_menu_items = array(
        __( 'Comments' ),
    );

    end( $menu );
    while ( prev( $menu ) ) {
        $item = explode( ' ', $menu[ key( $menu ) ][0] );
        if ( in_array( null !== $item[0] ? $item[0] : '', $remove_menu_items, true ) ) {
            unset( $menu[ key( $menu ) ] );
        }
    }
}
add_action( 'admin_menu', 'fx_remove_admin_menu_items' );

/**
 * Removes nodes from admin bar to make for white labeled
 *
 * @param  class $wp_toolbar the WordPress toolbar instance.
 * @return class             returns the modified toolbar
 */
function fx_remove_admin_bar_menus( $wp_toolbar ) {
    $wp_toolbar->remove_node( 'wp-logo' );
    return $wp_toolbar;
}
add_action( 'admin_bar_menu', 'fx_remove_admin_bar_menus', 999 );

/**
 * Remove the defualt dashboard widgets for orgs
 *
 * @return void
 */
function fx_remove_dashboard_widgets() {
    // remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' );
    // remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
    remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
}
add_action( 'wp_dashboard_setup', 'fx_remove_dashboard_widgets', 0 );

/**
 * Remove the WordPress text at the bottom of the admin
 *
 * @param  string $text current footer text.
 * @return string the changed footer text
 */
function fx_remove_footer_text( $text ) {
    return '';
}
add_filter( 'update_footer', 'fx_remove_footer_text', 999 );
add_filter( 'admin_footer_text', 'fx_remove_footer_text' );
