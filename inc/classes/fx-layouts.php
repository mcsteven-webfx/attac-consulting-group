<?php

function fx_layout_content() {
    load_template( FX_Layouts::$main_template );
}

function fx_layout_base() {
    return FX_Layouts::$base;
}

class FX_Layouts {
    /**
     * Stores the full path to the main template file
     * @var string
     */
    public static $main_template;

    /**
     * Stores the base name of the template file; e.g. 'page' for 'page.php' etc.
     * @var  string
     */
    public static $base;

    public static function wrap( $template ) {
        global $post;

        // stash the main template
        self::$main_template = $template;
        self::$base          = substr( basename( self::$main_template ), 0, -4 );

        if ( 'index' === self::$base ) {
            self::$base = false;
        }

        /**
         * Create template hierarchy layouts/{type}.php, layouts/master.php
         */
        $templates = array( 'layouts/master.php' );

        if ( self::$base ) {
            array_unshift( $templates, sprintf( 'layouts/%s.php', self::$base ) );
        }

        // Innerpage Layout
        if ( is_page() && !is_front_page() ) {
            array_unshift( $templates, 'layouts/page.php' );
        }

        // Post Layout
        if ( is_archive() || is_search() || is_single() ) {
            array_unshift( $templates, 'layouts/posts.php' );
        }

        if( is_front_page()  ){
            array_unshift( $templates, 'layouts/attac-homepage.php' );
        }

        if( is_page('contact') && !is_front_page() ){
            array_unshift( $templates, 'layouts/page.php' );
        }

        return locate_template( $templates );
    }
}
add_filter( 'template_include', array( 'FX_Layouts', 'wrap' ), 99 );
