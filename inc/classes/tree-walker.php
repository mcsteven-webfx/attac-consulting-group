<?php

class FX_Tree_Walker extends Walker_Nav_Menu
{

    private $menu_parent = -1;
    private $depth_passed = -1;


    // Only follow down one branch
    function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output )
    {
        // Check if element as a 'current element' class
        $current_element_markers = array( 'current-menu-item', 'current-menu-parent', 'current-menu-ancestor' );
        $current_class = array_intersect( $current_element_markers, $element->classes );

        // If element has a 'current' class, it is an ancestor of the current element
        $ancestor_of_current = !empty($current_class);

        // If this is a top-level link and not the current, or ancestor of the current menu item - stop here.
        if ( 0 == $depth && !$ancestor_of_current)
            return;

        // check to make sure we aren't adding any other top level nav items
        // makes sure you can't hit a 0 level again
        if($depth < $this->depth_passed && $depth < 1)
            return;

        // set the depth passed
        $this->depth_passed = $depth;

        parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }

}