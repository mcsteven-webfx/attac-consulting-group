<?php

// NOTE: to get custom post type archives/single post pages work, you may need to uncomment/tweak some of the commented out code below
//       if so, there is also a filter that will need to be uncommented/tweaked in "/inc/theme/extra.php"
class Segmented_Walker extends Walker_Nav_Menu {

    private $ID;
    private $depth;
    private $classes = array();
    private $child_count = 0;
    private $have_current = false;

    // Only follow down one branch
    function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {
        
        if ( $depth > 0 ) {
            parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
            return;
        }

        // Check if element is in the current tree to display
        $current_element_markers = array( 'current-menu-item', 'current-menu-parent', 'current-menu-ancestor' );
        // $current_element_markers = array( 'current-menu-item', 'current-menu-parent', 'current-menu-ancestor', 'current_page_parent' );
        
        // bug in wordpress that sets blog as page parent for custom post types
        // if (
        //     $element->title == 'Blog' && 
        //     (
        //         is_post_type_archive( 'case_study' )   || is_singular( 'case_study' )   ||
        //         is_post_type_archive( 'company_news' ) || is_singular( 'company_news' ) ||
        //         is_post_type_archive( 'article' )      || is_singular( 'article' )
        //     )
        // ) {
        //     $element->classes = array_diff( $element->classes, array( 'current_page_parent' ) );
        // }
        
        // for custom post archives and single posts
        // if (
        //     ( $element->title == 'Resources' || $element->title == 'Case Studies' ) && ( is_post_type_archive( 'case_study' )   || is_singular( 'case_study' ) )   ||
        //     ( $element->title == 'Resources' || $element->title == 'Company News' ) && ( is_post_type_archive( 'company_news' ) || is_singular( 'company_news' ) ) ||
        //     ( $element->title == 'Resources' || $element->title == 'Articles' )     && ( is_post_type_archive( 'article' )      || is_singular( 'article' ) )
        // ) {
        //     $element->classes[] = 'current_page_parent';
        // }
        
        $this->classes = array_intersect( $current_element_markers, $element->classes );
        
        // If element has a 'current' class, it is an ancestor of the current element
        $ancestor_of_current = !empty($this->classes);
        
        // check if the element is the actual page element we are on.
        $is_current = in_array('current-menu-item', $this->classes);

        // if it is the current element
        if($ancestor_of_current) {
            
            $output .= '<h3>' . $element->title . '</h3>';
            
            $id_field = $this->db_fields['id'];
            $id       = $element->$id_field;
            
            $this->has_children = ! empty( $children_elements[ $id ] );
            if ( isset( $args[0] ) && is_array( $args[0] ) ) {
                $args[0]['has_children'] = $this->has_children; // Back-compat.
            }

            if($this->has_children) {
                
                // if there are children loop through them and display the kids.
                foreach( $children_elements[$id] as $child ) {
                    parent::display_element( $child, $children_elements, $max_depth, $depth + 1, $args, $output );
                }
                unset( $children_elements[ $id ] );

            }
        }

        // if depth is zero and not in current tree go to the next element
        if ( ! $ancestor_of_current )
            return;
    
    }
}