<section class="blog-listing-container">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<?php get_template_part( 'partials/loop-content' ); ?>
<?php endwhile; endif; ?>
</section>

<?php get_template_part( 'partials/pagination' ); ?>
