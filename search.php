<?php
global $wp_query;

$output_loop = array();
$types       = array();
// add post types to exclude
$exclude = array('our_team', 'attachment');

foreach ( $wp_query->posts as $post ) {
    setup_postdata( $post );
    $post_type_data = get_post_type_object( get_post_type() );
    if ( ! in_array( $post_type_data->name, $exclude, true ) ) {
        $types[ $post_type_data->name ]         = $post_type_data;
        $output_loop[ $post_type_data->name ][] = $post;
    }
}

wp_reset_postdata();
?>
<!--  Inner Page Title Row  -->
<?php get_template_part( 'partials/inner-page-title' ); ?>  

<section class="page-content soft-ends soft-xs-ends soft-double-sm-ends soft-triple-md-ends" id="search-results">
    <?php if( $types || count( $types ) != 0 ) : ?>
    <div class="container">
        <ul class="tabs js-tabs">
        <?php $i = 0; ?>
        <?php foreach ( $types as $name => $post_type_data ) : ?>
            <li class="tab<?php echo 0 === $i ? ' tab-active show' : ''; ?>">
                <a href="#<?php echo $name; ?>"><?php echo $post_type_data->labels->name; ?></a>
            </li>
            <?php $i++; ?>
        <?php endforeach; ?>
        </ul>
        <?php $j = 0; ?>
        <?php foreach ( $types as $name => $post_type_data ) : ?>
            <section id="<?php echo $post_type_data->name; ?>" class="tab-content<?php echo 0 === $j ? ' show' : ''; ?> hard-sides soft-xs-top soft-double-sm-top soft-triple-md-top">
                <div class="container">
                    <div class="row">
<!--
                        <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-12">
                            <h3><?php echo $post_type_data->labels->name; ?></h3>
                        </div>
-->
                        <?php foreach ( $output_loop[ $name ] as $post ) : ?>
                            <?php setup_postdata( $post ); ?>
                            <?php get_template_part( 'partials/loop-content' ); ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            </section>
            <?php $j++; ?>
        <?php endforeach; ?>
        <?php wp_reset_postdata(); ?>
    </div>
    <?php else : ?>
    
    <div class="container no-result-container">
        <div class="row">
            <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-12">
                <h3 class="flush-top">Sorry, no results were found. Try using a different keywords.</h3>
                
                <div class="search-results-form-container">
                    <?php echo get_search_form(); ?>
                </div>
            </div>
        </div>
    </div>
    
    <?php endif; ?>
</section>

