<!--  Inner Page Title Row  -->
<?php get_template_part( 'partials/inner-page-title' ); ?>    

<?php 

/* Check if there are contents */
//require_once get_template_directory() . '/layouts/flexible-content.php';

?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<section class="page-content" id="article-content">
    <div class="container soft-triple-xs-ends  soft-triple-sm-ends  soft-triple-md-ends">
        <div class="row">
            <div class="col-xxs-12 col-xs-12 col-sm-4 col-md-3 sidebar-container soft-triple-xs-right soft-double-sm-right soft-triple-md-right">
                <?php require_once get_template_directory() . '/sidebar.php'; ?>
            </div>
            <?php if( get_post_type() == 'testimonial' ) : ?>
                <div class="col-xxs-12 col-xs-12 col-sm-8 col-md-9">
                    
                    <?php 
                        $hasVideoImage = '';
                        
                        if( get_the_post_thumbnail_url() ){
                            echo '<div class="post-image-banner pos-relative">';
                                echo fx_get_image_tag( get_the_post_thumbnail_url( get_the_ID(), 'full' ), 'image-background', false, 'full' ); 
                            echo '</div>';
                        }else if( get_field('testimonial_video') ){
                            echo '<div class="post-image-banner pos-relative">';
                                echo get_field('testimonial_video');
                            echo '</div>';
                        }else{
                            $hasVideoImage = 'flush-top';
                        }
                    ?>
                    
                    <h2 class="<?php echo $hasVideoImage; ?>"><?php the_title(); ?></h2>
                    <?php if( get_field('location') ) : ?>
                    <p><?php echo get_field('location'); ?></p>
                    <?php endif; ?>
                    <div class="bialty-container">
                        <?php 
                            $content = apply_filters('the_content', get_field('testimonial_content') );
                            $content = str_replace(']]>', ']]&gt;', $content);

                            echo $content;
                        ?>
                    </div>
                </div>
            <?php else: ?>
                <div class="col-xxs-12 col-xs-12 col-sm-8 col-md-9">
                    <div class="post-image-banner pos-relative">
                        <?php echo fx_get_image_tag( get_field('post_hero_image')['url'], 'image-background', false, 'full' ); ?>
                    </div>
                    <h2><?php the_title(); ?></h2>
                    <?php the_content(); ?>
                    <?php get_template_part( 'partials/social-share' ); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
    
<?php endwhile; endif; ?>