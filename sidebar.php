<aside class="page-sidebar">

    <?php
        // NOTE: example of using walker to get child pages
        //       see "/inc/classes/segmented-walker.php"
        // Output the main Menu
        // wp_nav_menu( array(
        //     'menu'            => 'main-menu',
        //     'container'       => '',
        //     'container_class' => '',
        //     'depth'           => 0,
        //     'walker'          => new Segmented_Walker()
        // ));
    ?>

    <?php dynamic_sidebar( 'sidebar' ); ?>

</aside>
