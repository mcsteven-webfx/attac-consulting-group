<!--  Inner Page Title Row  -->
<?php get_template_part( 'partials/inner-page-title' ); ?>

<!-- Add image buttons here for top ~4 pages - one should be the homepage, PM to specify the others -->

<!-- Image Buttons -->
<section class="page-content" id="layout-404">
    <div class="container">
        <div class="row">
            <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-12">
                <h2>Explore one of these pages instead:</h2>
            </div>
            <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-12 ">
                <div class="feat-services-container row">
                    <div class="col-xxs-12 col-xs-12 col-sm-4 col-md-4 soft soft-xs soft-sm">
                        <div class="service-data-container pos-relative">
                            <?php echo fx_get_image_tag( site_url() . '/wp-content/uploads/2020/11/provider-networks-feat-image.jpg','img-responsive', false, 'full' ); ?>

                            <div class="service-hovered">
                                <h3>Home</h3>
                                <a class="btn-tertiary-light" href="/">Learn More</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xxs-12 col-xs-12 col-sm-4 col-md-4 soft soft-xs soft-sm">
                        <div class="service-data-container pos-relative">
                            <?php echo fx_get_image_tag( site_url() . '/wp-content/uploads/2020/11/provider-networks-feat-image.jpg','img-responsive', false, 'full' ); ?>

                            <div class="service-hovered">
                                <h3>Services</h3>
                                <a class="btn-tertiary-light" href="<?php echo site_url(); ?>/our-service/">Learn More</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-xxs-12 col-xs-12 col-sm-4 col-md-4 soft soft-xs soft-sm">
                        <div class="service-data-container pos-relative">
                            <?php echo fx_get_image_tag( site_url() . '/wp-content/uploads/2020/11/provider-networks-feat-image.jpg','img-responsive', false, 'full' ); ?>

                            <div class="service-hovered">
                                <h3>Contact Us</h3>
                                <a class="btn-tertiary-light" href="<?php echo site_url(); ?>/contact">Learn More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-12 text-center soft soft-triple-xs soft-triple-sm soft-triple-md">
                <a href="<?php echo site_url(); ?>/contact" class="btn">Contact Us</a>
            </div>
        </div>
    </div>
</section>

<div class="container">

    <!-- Add sitemap shortcode here -->

</div>
