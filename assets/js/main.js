/* ---------------------------------------------------------------------
Global Js
Target Browsers: All
------------------------------------------------------------------------ */

var FX = ( function( FX, $ ) {

	/**
	 * Doc Ready
	 */
	$( function() {
		FX.General.init(); // For super general or super short scripts
		FX.ImAHuman.init(); // Enable by default
		FX.ExternalLinks.init(); // Enable by default
		FX.Tabs.init(); // Enable by default for site search page
        FX.Social.init(); // Open social share in new windows. Used in single.php by default
        FX.SlickSlider.init(); // Slick Slider
        FX.AttacCustomFunction.init();
        FX.BackToTop.init();
        FX.ATTACcheckPageIfScrolled.init();
		FX.Menu.init();

		// TODO: Add Modules needed for build. Remove unused modules
	});

	$( window ).on( 'load', function() {
		// TODO: Uncomment if using smooth scrolling anchors
		FX.SmoothAnchors.init();

		// setInterval(function() {
		//     $('#menu-item-54').addClass('ubermenu-active'); /* replace #menu-item-200 with the menu item ID your working on */
		// }, 1000 );

	});
	FX.Menu = {
		windowWidth: 0,

		init: function() {
			this.setMenuClasses();
			$( window ).on( 'resize', $.proxy( this.setMenuClasses, this ) );
			$( '.ubermenu-item-has-children' ).each( function() {
				$( this ).children( 'a' ).each( function() {
					let $this = $( this );
					$this.children( '.ubermenu-sub-indicator' ).clone().insertAfter( $this ).addClass( 'submenu-toggle hidden-md-up' );
					$this.children( '.ubermenu-sub-indicator' ).addClass( 'hidden-sm-down' );
				});
			});
			this.bind();
		},

		setMenuClasses: function() {
			let windowWidth = $( window ).innerWidth();
			// iOS fires resize event on scroll - let's first make sure the window width actually changed
			if ( windowWidth == this.windowWidth ) {
				return;
			}
			this.windowWidth = windowWidth;
			if ( this.windowWidth < 1025 ) {
				$( '.ubermenu-item-has-children' ).each( function() {
					$( this ).removeClass( 'ubermenu-has-submenu-drop' );
				});
			} else {
				$( '.ubermenu-item-has-children' ).each( function() {
					$( this ).addClass( 'ubermenu-has-submenu-drop' );
				});
			}
		},

		bind: function() {
			$( '.submenu-toggle' ).on( 'touchstart', this.toggleNextLevel );
		},

		toggleNextLevel: function( event ) {
			event.preventDefault();
			let $this = $( this );
			$this.toggleClass( 'fa-angle-down' ).toggleClass( 'fa-times' );
			$this.parent().toggleClass( 'ubermenu-active' );
			if ( $this.parent().hasClass( 'ubermenu-active' ) ) {
				$this.parent().siblings( '.ubermenu-active' ).removeClass( 'ubermenu-active' ).children( '.submenu-toggle' ).addClass( 'fa-angle-down' ).removeClass( 'fa-times' );
			}
		}


	}

    FX.AdjustInnerPageBG = {
        init: function(){
            if( $('.innerpage-title-bg-holder').length !== 0 ){
                if( !window.matchMedia('(max-width: 767px)').matches && !window.matchMedia('(max-width: 1024px)').matches ){
                    $pageTitleWidth = $('.page-title-container h2').width();

                    $newBGWidth = 'calc(1313px - calc( '+$pageTitleWidth+'px * 0.65))';
                    $newBGWidth2 = 'calc(1313px - 50px )';

                    if( $pageTitleWidth > 500 ){
                       $('.innerpage-title-bg-holder').css('max-width', $newBGWidth);
                    }else{
                        $('.innerpage-title-bg-holder').css('max-width', $newBGWidth2);
                    }
                }else if( window.matchMedia('(min-width: 767px)').matches && window.matchMedia('(max-width: 1024px)').matches ){
                    $pageTitleWidth = $('.page-title-container h2').width();

                    $newBGWidth = 'calc('+ $(window).width() +'px - calc( '+$pageTitleWidth+'px * 0.9 ) )';

                    $('.innerpage-title-bg-holder').css('max-width', $newBGWidth);
                }else{
                    $pageTitleWidth = $('.page-title-container h2').width();

                    $newBGWidth = 'calc('+ $(window).width() +'px - calc( '+$pageTitleWidth+'px * 0.7 ) )';

                    $('.innerpage-title-bg-holder').css('max-width', $newBGWidth);
                }
            }
        }
    };

    FX.ATTACcheckPageIfScrolled = {
        init: function(){
                if( !window.matchMedia('(max-width: 767px)').matches ){
                    if( $( window ).scrollTop() > 50 ) { // TODO: Update "100" for how far down page to show button
                        $('.page-header').addClass('is-scrolled');
                    } else {
                        $('.page-header').removeClass('is-scrolled');
                    }
                }

        }
    }

    FX.AttacCustomFunction = {
        init: function() {
            $( window ).resize(function(){
               FX.AdjustInnerPageBG.init();
            });

            $('.feat-services-container .service-data-container, .blog-listing-container .post-data-container').on('click', function(){
               window.location.href = $('.btn-tertiary', this).attr('href');
            });

            $( window ).on('scroll', function(){
               FX.ATTACcheckPageIfScrolled.init();

                if( $('.mobile-main-navigation-container').length !== 0 ){
                    if( window.matchMedia('(max-width: 767px)').matches ){
                        if( $( window ).scrollTop() > 200 ) { // TODO: Update "100" for how far down page to show button
//                            $('.mobile-main-navigation-container').slideDown();
                        } else {

                            $('.searchwp-live-search-results').removeClass('searchwp-live-search-results-showing');
                            $('.header-search-form-container .container form input').val('');
//                            $('.mobile-main-navigation-container').slideUp();
                        }
                    }
                }
            });

            $('.header-search-btn-trigger, .mobile-nav-search-btn-trigger').on('click', function(){
                $('.header-search-form-container').toggleClass('active');
                if( !window.matchMedia('(max-width: 767px)').matches ){
                   $('.page-header').toggleClass('search-open');
                }


                if( $('.header-search-form-container').hasClass('active') ){
                    $('.header-search-form-container').slideDown();
                }else{
                    $('.header-search-form-container').slideUp();
                }
            });

            $( document ).ready(function(){

                setTimeout(function(){
                    FX.AdjustInnerPageBG.init();
                }, 1000);
            });

            $('header.desktop-only .mega-submenu-item').on('mouseenter', function(){
                $submenuKey = $(this).attr('data-submenu-key');

                $('header.desktop-only .mega-submenu-submenu-item-wrapper').each(function(){
                    if( $(this).attr('data-submenu') === $submenuKey ){
                        $('.mega-submenu-submenu-item-wrapper').hide().removeClass('active');

                        $(this).show().addClass('active');
                    }
                });
            });

            $('.team-member-container').on('click', function(){
               if( !$(this).hasClass('active') ){
                   $('.team-member-bio-container').slideUp();
                   $('.team-member-container').removeClass('active');
                   $('.team-member-bio-container', this).slideDown();

                   $(this).addClass('active');

                   $('a.btn-tertiary span:first-child',this).html('Close Bio');
               }else{
                   $(this).removeClass('active');
                   $('a.btn-tertiary span:first-child',this).html('View Full Bio');
                   $('.team-member-bio-container', this).slideUp();
               }
            });

            /* DESKTOP MENU */
            $('.page-header-menu-wrapper ul#menu-main-menu > li > .sub-menu > li > .sub-menu > li').hover( function(){
                if( $('> .sub-menu', this).length !== 0 ){
                   $('.page-header-menu-wrapper ul#menu-main-menu > li > .sub-menu').css('left', 'calc(20% + 4px)');
                }
            }, function(){
                $('.page-header-menu-wrapper ul#menu-main-menu > li > .sub-menu').attr('style', '');
            });

            /* TABLET MENU */
            $('.mega-submenu-item > a').on('click', function(e){
                if( $(this).closest('.mega-submenu-item').hasClass('has-submenu') && !$(this).closest('.mega-submenu-item').hasClass('active') ){
                    e.preventDefault();

                    if( $(this).closest('.mega-submenu-item').find('> .sub-menu').length !== 0 ){

                        if( $(this).closest('.sub-menu').length === 0 ){
                            $(this).closest('ul.menu').find('.mega-submenu-item').removeClass('active');
                            $(this).closest('ul.menu').find('.mega-submenu-item > .sub-menu').slideUp();
                        }

                        $(this).closest('.sub-menu').find('.mega-submenu-item').removeClass('active');
                        $(this).closest('.sub-menu').find('.mega-submenu-item > .sub-menu').slideUp();

                        $(this).closest('.mega-submenu-item').addClass('active').find('> .sub-menu').slideDown();
                    }
                }else{
                    $(this).closest('.mega-submenu-item').removeClass('active');
                    $(this).closest('.mega-submenu-item').find('> .sub-menu').slideUp();
                }
            });

            $('.tablet-nav > ul.menu > li.menu-item > a').on('click', function(e){
                if( !$(this).hasClass('active') && $(this).closest('li').hasClass('has-submenu') ){
                    e.preventDefault();
                    $('.tablet-nav > ul.menu > li.menu-item > a').removeClass('active');
                    $(this).addClass('active');
                }else{
                    window.location.href = $(this).attr('href');
                }
            });

            /* MOBILE MENU OVERLAY */
            $('.mobile-overlay-menu-container ul.menu .menu-item-has-children a').on('click', function(e){
               if( !$(this).closest('.menu-item-has-children').hasClass('active') ){
                   e.preventDefault();

                   if( $(this).closest('.sub-menu').length === 0 ){
                        $(this).closest('ul.menu').find('> .menu-item-has-children').removeClass('active');
                        $(this).closest('ul.menu').find('> .menu-item-has-children > .sub-menu').slideUp();
                    }

                   $(this).closest('.sub-menu').find('> .menu-item-has-children').removeClass('active');
                   $(this).closest('.sub-menu').find('> .menu-item-has-children > .sub-menu').slideUp();
                   $(this).closest('.menu-item-has-children').addClass('active').find('> .sub-menu').slideDown();
               }else{
                   $(this).closest('.menu-item-has-children').removeClass('active').find('> .sub-menu').slideUp();
               }
            });

            $('.mobile-menu-hamburger-btn').on('click', function(){
                if( !$(this).hasClass('active') ){
                    $(this).addClass('active');

                    $('.mobile-overlay-menu-container').slideDown();
                }else{
                    $(this).removeClass('active');

                    $('.mobile-overlay-menu-container, .mobile-overlay-menu-container .menu-item-has-children .sub-menu').slideUp();
                    $('.mobile-overlay-menu-container .menu-item-has-children').removeClass('active');
                }
            });
        }
    };

	/**
	 * General functionality — ideal for one-liners or super-duper short code blocks
	 */
	FX.General = {
		init: function() {
			this.bind();
		},

		bind: function() {

			// Makes all PDF to open in new tabs
			$('a[href*=".pdf"]').each(function(e) {
				$(this).attr('target', '_blank');
			});

			// Choices (select field styling) - additional options here https://github.com/jshjohnson/Choices
            if ( $('select').length ) {
                $('select').each(function(){
                    FX.General.setUpChoice(this);
                });


                $('select#cat').on('change', function(){
                    $(this).closest('form').submit();
                });
            }

			// pretty-format phone inputs
			$('[type="tel"]').phoneFormat();

			// position: sticky support through polyfill
			var $stickyEls = $('.js-sticky');
			if( $stickyEls.length && typeof( Stickyfill ) === 'object' )
				Stickyfill( $stickyEls );

			// FitVids - responsive videos
			$('body').fitVids();

			// TODO: Add additional small scripts below
		},
		setUpChoice: function (elSelect) {
    		const choice = new Choices( elSelect )
            const form = elSelect.closest('form')

    		form.addEventListener('reset', function () {
    			choice.destroy(); // destroy old choices instance

    			FX.General.setUpChoice(elSelect); // rebuild new choices instance
    		}, {once : true});

    		if( 'cat' === elSelect.id ) {
    			elSelect.onchange = null

    			choice.passedElement.element.addEventListener( 'change', e => {
    				e.target.closest('form').submit()
    			})
    		}
    	}
	};

	/**
	 * Slider/Carousel
	 * @type {Object}
	 */
	FX.SlickSlider = {
		init: function() {
			// Fix flashing issue (first slide initially shown)
			$( '.slick-slider' ).on( 'init', function( e, slick ) {
				$( '.slideshow .slide' ).show();
			});

			/* Preloader */
			$( '.js-slider-has-preloader' ).on( 'init', function( e, slick ) {
				$( '.js-slider-has-preloader' ).addClass( 'js-slider-has-preloader-init' );
			});

            $('.slick-slider').each(function(){
                if( $(this).attr('data-slick-slidesToShow') > 1 ){
                    $responsiveSlidestoShow = 2;
                }else{
                    $responsiveSlidestoShow = $(this).attr('data-slick-slidesToShow');
                }

                if( $(this).attr('data-slick-autoplay') === true || $(this).attr('data-slick-autoplay') === 'true'){
                    $autoPlay = true;
                }else{
                    $autoPlay = false;
                }

                $(this).slick({
                    infinite: true,
                    slidesToShow: $(this).attr('data-slick-slidesToShow'),
                    slidesToScroll: 1,
										autoplaySpeed: 5000,
                    autoplay: $autoPlay,
                    prevArrow: '<button type="button" class="slick-prev attac-navarrow"><span class="icon-Arrow"></span></button>',
                    nextArrow: '<button type="button" class="slick-next attac-navarrow"><span class="icon-Arrow"></span></button>',
                    responsive: [
                        {
                          breakpoint: 1025,
                          settings: {
                            slidesToShow: $responsiveSlidestoShow,
                            slidesToScroll: 1
                          }
                        },
                        {
                          breakpoint: 767,
                          settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                          }
                        }
                    ]
                });
            });

			$('.new-slider_content-wrapper').slick({
				arrows: true,
				autoplay: true,
				autoplaySpeed: 4000,
			});
		}
	};

	/**
	 * Display scroll-to-top after a certain amount of pixels
	 * @type {Object}
	 */
	FX.BackToTop = {
		$btn: null,

		init: function() {
			this.$btn = $('.back-to-top');
			if( this.$btn.length )
				this.bind();
		},

		bind: function() {
			$(window).on( 'scroll load', this.maybeShowButton.bind( this ) );
			this.$btn.on( 'click', this.scrollToTop );
		},

		maybeShowButton: function() {
			if( $( window ).scrollTop() > 100 ) { // TODO: Update "100" for how far down page to show button
				this.$btn.removeClass( 'hide' );
			} else {
				this.$btn.addClass( 'hide' );
			}
		},

		scrollToTop: function() {
//			$(window).scrollTop( 0 );
            $('body,html').animate({ scrollTop: 0 }, 500);
		}
	};

	/**
	 * Mobile menu script for opening/closing menu and sub menus
	 * @type {Object}
	 */
	FX.MobileMenu = {
		init: function() {
			$( '.nav-primary li.menu-item-has-children > a' ).after( '<span class="sub-menu-toggle icon-arrow-down hidden-md-up"></span>' );
			$( '.sub-menu-toggle' ).click( function() {
				var $this = $(this),
					$parent = $this.closest( 'li' ),
					$wrap = $parent.find( '> .sub-menu' );
				$wrap.toggleClass( 'js-toggled' );
				$this.toggleClass( 'js-toggled' );
			});
		}
	};

	/**
	 * Force External Links to open in new window.
	 * @type {Object}
	 */
	FX.ExternalLinks = {
		init: function() {
			var siteUrlBase = FX.siteurl.replace( /^https?:\/\/((w){3})?/, '' );
			$( 'a[href*="//"]:not([href*="'+siteUrlBase+'"])' )
				.not( '.ignore-external' ) // ignore class for excluding
				.addClass( 'external' )
				.attr( 'target', '_blank' )
				.attr( 'rel', 'noopener' );
		}
	};

	/**
	 * Responsive Tables
	 * @type {Object}
	 */
	FX.ResponsiveTables = {

		init: function() {
			this.bind();
		},

		bind: function() {
			var self = this;

			// Add wrappers to table
			// - change ".page-content table" to appropriate class per project
			$( '.page-content table' ).wrap( '<div class="table-wrap-outer"><div class="table-wrap-inner"></div></div>' );

			// Make table draggable
			var mx = 0;
			$( '.table-wrap-inner' ).on({
				mousemove: function( e ) {
					var mx2 = e.pageX - this.offsetLeft;
					if ( mx ) {
						this.scrollLeft = this.sx + mx - mx2;
					}
				},
				mousedown: function( e ) {
					this.sx = this.scrollLeft;
					mx      = e.pageX - this.offsetLeft;
				}
			});

			$( document ).on( 'mouseup', function() {
				mx = 0;
			});

			// Add class if table is wider than parent
			$( '.table-wrap-outer' ).find( '.table-wrap-inner table' ).each( function() {
				var $table 			= $( this ),
					$table_outer 	= $table.closest( '.table-wrap-outer' );
				if ( $table.width() > $table_outer.width() ) {
					$table_outer.addClass( 'js-table-is-overflowing' );
					$( '.page-content table' ).before( '<div class="js-table-fade"></div>' );
				}
			});

		}
	};

	/**
	 * Custom Social Share icons open windows
	 * Generate URLs, place in a tag and use class - example: https://github.com/bradvin/social-share-urls
	 * @type {Object}
	 */
	FX.Social = {
		init: function() {
			$( '.js-social-share' ).on( 'click', this.open );
		},

		open: function( e ) {
		  e.preventDefault();
		  FX.Social.windowPopup( $( this ).attr( 'href' ), 500, 300 );
		},

		windowPopup: function( url, width, height ) {
			var left = ( screen.width / 2 ) - ( width / 2 ),
				top  = ( screen.height / 2 ) - ( height / 2 );

			window.open(
				url,
				'',
				'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=' + width + ',height=' + height + ',top=' + top + ',left=' + left
			);
		}
	};

	/**
	 * ImAHuman
	 * Hidden Captchas for forms
	 * @type {Object}
	 */
	FX.ImAHuman = {
		init: function() {
			this.bind();
		},

		bind: function() {
			var self = this,
				$forms = $('form');

			$forms.each( function() {
				$(this).on( 'focus click', self.markAsHuman );
			});
		},

		markAsHuman: function() {
			$(this).find( '.imahuman, [name="imahuman"]' ).attr( 'value', parseInt( '0xFF9481', 16 ) );
		}
	};


	/**
	 * Affix
	 * Fixes sticky items on scroll
	 * @type {Object}
	 */
	FX.Affix = {
		$body: 			null,
		$header: 		null,
		headerHeight: 	null,
		scrollTimeout: 	null,
		resizeTimeout: 	null,

		init: function() {
			this.$body 			= $(document.body);
			this.$header 		= $('.page-header');
			this.headerHeight 	= this.$header.outerHeight( true );

			this.bind();
        },

        bind: function(e) {
			$(window).on( 'scroll', this.handleScroll.bind( this ) );
			$(window).on( 'resize', this.handleResize.bind( this ) );
		},

		handleScroll: function( e ) {
			var self = this;

			// avoid constantly running intensive function(s) on scroll
			if( self.scrollTimeout !== null )
				clearTimeout( self.scrollTimeout );

			setTimeout( function() {
				self.maybeAffixHeader();
			}, 500 );
		},

		handleResize: function( e ) {
			var self = this;

			// avoid constantly running intensive function(s) on resize
			if( self.resizeTimeout !== null )
				clearTimeout( self.resizeTimeout );

			setTimeout( function() {
				self.headerHeight = self.$header.outerHeight( true );
			}, 500 );
		},

		maybeAffixHeader:function() {
			var self = this;

			if( 200 < $(window).scrollTop() ) {
				self.$body.css( 'padding-top', self.headerHeight );
				self.$header.addClass('js-scrolled');
			} else {
				self.$body.css( 'padding-top', 0 );
				self.$header.removeClass('js-scrolled');
			}
		}
	};


	/**
	 * FX.Parallax
	 * Parallax effect for images
	 * @type {Object}
	 */
	FX.Parallax = {

		init: function() {
			this.bind();
		},

		bind: function() {
			$( window ).scroll( this.scroll );
		},

		scroll: function( e ) {
			$( '.js-parallax' ).each( function() {
				var $this   = $( this ),
					speed   = $this.data( 'speed' ) || 6,
					yPos    = -( $( window ).scrollTop() / speed ),
					coords  = 'center  '+ yPos + 'px';

				$this.css( { objectPosition: coords } ); /* based on parallax using an object-fit <img> */
			});
		}
	};

	/**
	 * FX.SmoothAnchors
	 * Smoothly Scroll to Anchor ID
	 * @type {Object}
	 */
	FX.SmoothAnchors = {
		init: function() {
			this.hash = window.location.hash;
			if ( this.hash != '' ) {
				this.scrollToSmooth( this.hash );
			}
			this.bind();
		},

		bind: function() {
			$( 'a[href^="#"]' ).on( 'click', $.proxy( this.onClick, this ) );
		},

		onClick: function( e ) {
			e.preventDefault();
			var target = $( e.currentTarget ).attr( 'href' );
			this.scrollToSmooth( target );
		},

		scrollToSmooth: function( target ) {
			var $target = $( target );
			$target = ( $target.length ) ? $target : $( this.hash );

			var headerHeight = $('.page-header').outerHeight(true);

			if ( $target.length ) {
				var targetOffset = $target.offset().top - headerHeight - 50;
				$( 'html, body' ).animate( { scrollTop: targetOffset }, 600 );
				return false;
			}
		}
	};

	/**
	 * Tab Content
	 * @type {Object}
	 */

	FX.Tabs = {
		init: function() {
			$( '.js-tabs' ).on( 'click touchstart', 'a', this.switchTab );
		},
		switchTab: function( e ) {
			e.preventDefault();
			var $this = $( this ),
				$tab  = $( $this.attr( 'href' ) );

			$this.parent()
				 .addClass( 'tab-active show' )
				 .siblings()
				 .removeClass( 'tab-active show' );

			$tab.addClass( 'tab-active show' )
				.siblings()
				.removeClass( 'tab-active show' );
		}
	};

	return FX;

}(FX || {}, jQuery));
