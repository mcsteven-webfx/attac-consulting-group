<?php
/**
 * If your form is generated using get_search_form() you do not need to do this,
 * as SearchWP Live Search does it automatically out of the box
 */
?>
<form action="/" method="get">
    <input type="text" name="s" value="" placeholder="Enter your search term ..."/> <!-- data-swplive="true" enables SearchWP Live Search -->
    <button class="header-search-btn" type="submit">
        <span class="icon-Search"></span>
    </button>
</form>
