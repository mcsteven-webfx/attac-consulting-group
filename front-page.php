<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <?php if ( have_rows( 'fp_slides' ) ) : ?>
        <header class="masthead masthead--slideshow" id="masthead">
        <?php while ( have_rows( 'fp_slides' ) ) : the_row(); ?>
            <div>

                <?php
                    $img = get_field('image'); //ACF array
                    echo fx_get_image_tag( $img['url'], ['img-responsive'], true, 'full' );
                ?>
                <h2><?php the_sub_field( 'title' ); ?></h2>
                <a href="<?php the_sub_field( 'button_link' ); ?>"><?php the_sub_field( 'button_label' ); ?></a>
            </div>
        <?php endwhile; ?>
        </header>
    <?php endif; ?>
        <article class="page-content">
            <!-- Sections should contain h tags -->
            <h2>Section</h2>
            <?php the_content(); ?>
        </article>
<?php endwhile; endif; ?>
