<?php get_header(); ?>

<main id="page-body" <?php post_class( 'page-body' ); ?>>

    <!--  Inner Page Title Row  -->
    <?php get_template_part( 'partials/inner-page-title' ); ?>


    <!--  Contact Us Details  -->
    <section class="page-content pos-relative" id="contact-us">

        <div class="container">
            <div class="row">
                <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-4">
                    <div class="contact-detail-container">
                        <?php if( get_field('left_heading') ) : ?>
                        <h3><?php echo get_field('left_heading'); ?></h3>
                        <?php endif; ?>

                        <?php if( get_field('address') ) : ?>
                        <p><?php echo get_field('address'); ?></p>
                        <?php endif; ?>

                        <?php if( get_field('phone') || get_field('fax') || get_field('email') ) : ?>
                        <div class="contact-details">
                            <?php if( get_field('phone') ) :
                                $phone = preg_replace( '/[^A-Za-z0-9\-]/', '', str_replace(' ', '', get_field('phone') ) );
                            ?>
                                <div class="contact-detail-item">
                                    <span class="contact-detail-item-icon icon-Phone"></span> <span><?php echo get_field('phone'); ?></a></span>
                                </div>
                            <?php endif; ?>

                            <?php if( get_field('fax') ) :
                                $fax = preg_replace( '/[^A-Za-z0-9\-]/', '', str_replace(' ', '', get_field('fax') ) );
                            ?>
                                <div class="contact-detail-item">
                                    <span class="contact-detail-item-icon icon-Fax"></span> <span><?php echo get_field('fax'); ?></a></span>
                                </div>
                            <?php endif; ?>

                            <?php if( get_field('email') ) : ?>
                                <div class="contact-detail-item">
                                    <span class="contact-detail-item-icon icon-Email"></span> <span><?php echo get_field('email'); ?></a></span>
                                </div>
                            <?php endif; ?>
                        </div>
                        <?php endif; ?>
                    </div>
                    <div class="contact-address-map-container">
                        <iframe width="400" height="290" src="https://maps.google.it/maps?q=<?php echo str_replace(' ', '', get_field('address')); ?>&output=embed"></iframe>
                    </div>

                  <?php the_field('disclaimers'); ?>  

                </div>
                <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-8">
                    <div class="contact-form-container">
                        <?php
                            echo do_shortcode( get_field('contact_form_shortcode') );
                        ?>
                    </div>
                </div>


            </div>
        </div>
    </section>
    <?php

    /* Check if there are contents */
    require_once get_template_directory() . '/layouts/flexible-content.php';

    ?>
</main>

<?php get_footer(); ?>
