<?php get_header(); ?>

<main id="page-body" <?php post_class( 'page-body' ); ?>>
    
<?php fx_layout_content(); ?>

</main>
    
<?php get_footer(); ?>
