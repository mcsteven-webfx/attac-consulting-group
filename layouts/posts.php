<?php get_header(); ?>

<main id="page-body" <?php post_class( 'page-body' ); ?>>
<!--
<?php get_template_part( 'partials/masthead' ); ?>
-->
    <article class="page-content">
        <h2 class="hide">Contents</h2>
        <?php fx_layout_content(); ?>
        
    </article>

</main>
    
<?php get_footer(); ?>
