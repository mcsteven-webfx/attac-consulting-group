<?php

/* Check if there are contents */
if( have_rows('flexible_content') ){

    while( have_rows('flexible_content') ) : the_row();

        /* Introduction Text/Image */
        if( get_row_layout() == 'introduction_textimage' ){
            get_template_part( 'partials/introduction-text-image' );
        }

        /* Image Buttons */
        if( get_row_layout() == 'image_buttons' ){
            get_template_part( 'partials/image-buttons' );
        }

        /* Client Testimonial */
        if( get_row_layout() == 'testimonial_section' ){
            get_template_part( 'partials/client-testimonial' );
        }

        /* Newsletter */
        if( get_row_layout() == 'newsletter' ){
            get_template_part( 'partials/newsletter' );
        }

        /* CTA Banner */
        else if( get_row_layout() == 'cta_section' ){
            get_template_part( 'partials/cta-section' );
        }

        /* WYSIWYG */
        else if( get_row_layout() == 'wysiwyg_section' ){
            get_template_part( 'partials/info-section' );
        }

        /* Image with a Fade */
        else if( get_row_layout() == 'section_image_with_fade' ){
            get_template_part( 'partials/image-with-fade' );
        }

        /* Image and Text */
        else if( get_row_layout() == 'image_and_text_section' ){
            get_template_part( 'partials/image-and-text' );
        }

        /* Team Section */
        else if( get_row_layout() == 'team_section' ){
            get_template_part( 'partials/team-section' );
        }

        /* Team Section */
        else if( get_row_layout() == 'toc_bar' ){
            get_template_part( 'partials/toc-bar' );
        }

        /* Card Services Section */
        else if( get_row_layout() == 'card_services_section' ){
            get_template_part( 'partials/card-services-section' );
        }

    endwhile;
}else{
    get_template_part( 'partials/content-only' );
}

?>
