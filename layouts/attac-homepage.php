<?php get_header(); ?>
<?php
    $svgArrow = '<svg xmlns="http://www.w3.org/2000/svg" width="61" height="100" viewBox="0 0 61 100"><path id="Polygon_1" data-name="Polygon 1" d="M50,0l50,61H0Z" transform="translate(61 0) rotate(90)"/></svg>';
?>

<main id="page-body" <?php post_class( 'page-body' ); ?>>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
    <?php endwhile; endif; ?>
</main>

<?php get_footer(); ?>
