<?php get_header(); ?>
<?php
    $arrowSVG = '<svg xmlns="http://www.w3.org/2000/svg" width="61" height="100" viewBox="0 0 61 100"><path id="Polygon_1" data-name="Polygon 1" d="M50,0l50,61H0Z" transform="translate(61 0) rotate(90)"></path></svg>';

    $carretSVG = '<svg xmlns="http://www.w3.org/2000/svg" width="14" height="8" viewBox="0 0 14 8">
      <path id="Polygon_25" data-name="Polygon 25" d="M7,0l7,8H0Z" fill="#242852"/>
    </svg>
    ';
?>

<main id="page-body" <?php post_class( 'page-body' ); ?>>

    <!--  Inner Page Title Row  -->
    <?php get_template_part( 'partials/inner-page-title' ); ?>

    <section class="page-content pos-relative" id="meet-the-team-banner">
        <?php
            echo fx_get_image_tag( get_the_post_thumbnail_url(), 'image-background', false, 'full' );
        ?>
        <div class="container">
            <div class="row">
                <div class="col-xxs-12 col-xs-12 col-sm-5 col-md-4">

                </div>
                <div class="col-xxs-12 col-xs-12 col-sm-7 col-md-8">
                    <div class="pos-relative meet-the-team-banner-text-container">
                        <?php
                            echo  fx_get_image_tag( site_url() .'/wp-content/uploads/2020/11/three-arrows-horizontal.png','img-responsive img-mobile-only', false, 'full' );

                            echo fx_get_image_tag( site_url() .'/wp-content/uploads/2020/11/three-arrows-vertical.png','img-responsive img-desktop-tablet-only', false, 'full' );
                        ?>

                        <div class="meet-the-team-banner-text-wrapper">
                            <?php
                                $content = apply_filters('the_content', get_the_content() );
                                $content = str_replace(']]>', ']]&gt;', $content);

                                echo $content;
                            ?>

                            <?php
                            if( get_field('include_a_button_on_banner') ) {
                                if( get_field('banner_button_text') && get_field('banner_button_link') ) {
                                    echo '<div class="banner-cta-button"><a class="btn" href="'. get_field('banner_button_link') .'">'. get_field('banner_button_text') .'</a></div>';
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="page-content" id="leadership-team">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php echo fx_get_image_tag( site_url() . '/wp-content/uploads/2020/11/three-arrows-horizontal.png', 'img-responsive horizontal-arrows', false, 'full' ); ?>
                    <h2 class="h1">Leadership Team</h2>
                </div>

                <div class="leadership-team-container display-flex col-md-12">
                    <?php
                        $args = array(
                            'post_type' => 'our_team',
                            'post_status'   => 'publish',
                            'posts_per_page' => -1,
                            'order' => 'ASC',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'team_category',
                                    'field'    => 'slug',
                                    'terms'    => 'leadership-team',
                                ),
                            ),
                        );

                        $theQuery = new WP_Query( $args );

                        if( $theQuery->have_posts() ) {
                            while( $theQuery->have_posts() ) : $theQuery->the_post(); ?>

                            <div class="team-member-container">
                                <div class="team-member pos-relative">
                                    <div class="pos-relative img-bio-container">
                                        <?php echo fx_get_image_tag( get_the_post_thumbnail_url(), 'img-responsive', false, 'full' ); ?>

                                        <div class="team-member-overlay-bio">
                                            <a class="btn-tertiary"><span>View Full Bio</span> <span><?php echo $arrowSVG; ?></span></a>
                                        </div>
                                    </div>
                                    <div class="team-member-details pos-relative">
                                        <h4><?php echo get_the_title(); ?></h4>
                                        <p class="team-position"><?php echo get_field('position'); ?></p>
                                        <?php if( get_field('department') ) : ?>
                                            <p class="team-department"><?php echo get_field('department'); ?></p>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="team-member-bio-container">
                                    <h4><?php echo get_the_title(); ?></h4>
                                    <div class="team-position-container">
                                        <span class="team-position"><?php echo get_field('position'); ?></span>
                                        <?php if( get_field('department') ) : ?>
                                            <span class="team-department"><?php echo get_field('department'); ?></span>
                                        <?php endif; ?>
                                    </div>

                                    <?php if( get_the_content() ) : ?>
                                    <div class="team-description-container">
                                        <?php
                                            $content = apply_filters('the_content', get_the_content() );
                                            $content = str_replace(']]>', ']]&gt;', $content);

                                            echo $content;
                                        ?>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>

                    <?php   endwhile;
                            wp_reset_postdata();
                        }
                    ?>
                </div>
            </div>
        </div>
    </section>

    <section class="page-content" id="consultant-team">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php echo fx_get_image_tag( site_url() . '/wp-content/uploads/2020/11/three-arrows-horizontal.png', 'img-responsive horizontal-arrows', false, 'full' ); ?>
                    <h2 class="h1">Consultants</h2>
                </div>

                <div class="leadership-team-container display-flex col-md-12">
                    <?php
                        $args = array(
                            'post_type' => 'our_team',
                            'post_status'   => 'publish',
                            'posts_per_page' => -1,
                            'order' => 'ASC',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'team_category',
                                    'field'    => 'slug',
                                    'terms'    => 'consultants',
                                ),
                            ),
                        );

                        $theQuery = new WP_Query( $args );

                        if( $theQuery->have_posts() ) {
                            while( $theQuery->have_posts() ) : $theQuery->the_post(); ?>

                            <div class="team-member-container">
                                <div class="team-member pos-relative">
                                    <div class="team-member-details">
                                        <h4><?php echo get_the_title(); ?></h4>
                                        <p class="team-position"><?php echo get_field('position'); ?></p>
                                        <?php if( get_field('department') ) : ?>
                                            <p class="team-department"><?php echo get_field('department'); ?></p>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="team-member-bio-container">
                                    <h4><?php echo get_the_title(); ?></h4>
                                    <div class="team-position-container">
                                        <span class="team-position"><?php echo get_field('position'); ?></span>
                                        <?php if( get_field('department') ) : ?>
                                            <span class="team-department"><?php echo get_field('department'); ?></span>
                                        <?php endif; ?>
                                    </div>

                                    <?php if( get_the_content() ) : ?>
                                    <div class="team-description-container">
                                        <?php
                                            $content = apply_filters('the_content', get_the_content() );
                                            $content = str_replace(']]>', ']]&gt;', $content);

                                            echo $content;
                                        ?>
                                    </div>
                                    <?php endif; ?>

                                    <div class="team-member-close-bio-container">
                                        <div class="team-member-close-bio-btn">Close Bio <?php echo $carretSVG; ?></div>
                                    </div>
                                </div>
                            </div>

                    <?php
                            endwhile;
                            wp_reset_postdata();
                        }
                    ?>
                </div>
            </div>
        </div>
    </section>

</main>

<?php get_footer(); ?>
