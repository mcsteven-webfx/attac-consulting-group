<?php get_header(); ?>

<main id="page-body" <?php post_class( 'page-body' ); ?> >

    <article class="page-content">
        <!--  Inner Page Title Row  -->
        <?php get_template_part( 'partials/inner-page-title' ); ?>

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile; endif; ?>
    </article>

</main>


<?php get_footer(); ?>
