<!--  Inner Page Title Row  -->
<?php get_template_part( 'partials/inner-page-title' ); ?>    

<?php $pagination = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1; $hasFeatured = false; ?>
<?php if ( 1 === $pagination ) : // Only show Featured post on first page of posts ?>
    <?php
    $featured_post_query = new WP_Query(
        [
            'posts_per_page' => 1,
            'meta_key'       => 'post_featured',
            'meta_value'     => '1',
        ]
    );
    ?>
    <?php if ( $featured_post_query->have_posts() ) : while ( $featured_post_query->have_posts() ) : $featured_post_query->the_post(); $hasFeatured = true; ?>
        <!-- featured post HTML goes here -->
        <section class="page-content soft-ends soft-xs-ends soft-double-sm-ends soft-triple-md-ends" id="featured-article">
            <div class="container">
                <div class="row">
                    <div class="col-xxs-12 col-xs-12 col-sm-5 col-md-4">
                        <div class="pos-relative the-image-container">
                            <?php echo fx_get_image_tag( get_field('post_hero_image')['url'],'image-background', false, 'full' ); ?>
                        </div>
                    </div>
                    <div class="col-xxs-12 col-xs-12 col-sm-7 col-md-8">
                        <div class="the-text-container left soft-bottom">
                            <p class="post-featured-tag">Featured Article</p>
                            <h2 class="h1"><?php echo get_the_title(); ?></h2>
                            <p class="post-item__meta"><?php echo get_the_date(); ?></p>
                            <div class="the-text-content">
                                <?php 
                                /* Get content and sanitize */
                                $content = apply_filters('the_content', wp_trim_words( get_the_content(), 20 ) );
                                $content = str_replace(']]>', ']]&gt;', $content);

                                echo $content;
                                ?>
                            </div>

                            <div class="image-and-text-btn-container">
                                <a class="btn" href="<?php echo get_the_permalink(); ?>">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
    <?php endwhile; endif; wp_reset_postdata(); ?>
<?php endif; ?>

<section class="page-content blog-listing" style="background-color: <?php echo get_field( 'background', get_option('page_for_posts', true) ); ?>;">
    <div class="container">
        <div class="row">
            <div class="col-xxs-12 col-xs-12 col-sm-4 col-md-3 sidebar-container soft-triple-xs-right soft-double-sm-right soft-triple-md-right">
                <?php require_once get_template_directory() . '/sidebar.php'; ?>
            </div>
            <div class="col-xxs-12 col-xs-12 col-sm-8 col-md-9">
                <div class="blog-listing-container row">
                    <?php                             
                        $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

                        $args = array(
                            'post_type' => 'post',
                            'post_status'   => 'publish',
                            'posts_per_page' => get_option( 'posts_per_page' ),
                            'paged' => $paged
                        );

                        $theQuery = new WP_Query( $args );

                        if( $theQuery->have_posts() ){
                            while( $theQuery->have_posts() ) : $theQuery->the_post(); ?>

                            <?php get_template_part( 'partials/loop-content' ); ?>

                        <?php
                            endwhile;
                        }

                        wp_reset_postdata();
                    ?>
                </div>
                <div class="blog-listing-pagination-container">
                    <?php get_template_part( 'partials/pagination' ); ?>
                </div>
            </div>
        </div>
    </div>
</section>