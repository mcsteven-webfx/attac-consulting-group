<?php 

// check whether the user/admin selected a category to display
if( get_sub_field('testimonial_category') ){
    $args = array(
        'post_type' => 'testimonial',
        'post_status'   => 'publish',
        'posts_per_page' => -1,
        'tax_query' => array(
            array(
                'taxonomy' => 'team_category',
                'field'    => 'term_id',
                'terms'    => get_sub_field('testimonial_category'),
            ),
        ),
    );
}else{ // if no category is selected
    $args = array(
        'post_type' => 'testimonial',
        'post_status'   => 'publish',
        'posts_per_page' => -1,
    );
}

$theQuery = new WP_Query( $args );

// IF there are testimonial to display
if( $theQuery->have_posts() ) : ?>

<!--  TESTIMONIAL  -->
<section class="page-content pos-relative testimonial-section" id="testimonial-section">
    
    <?php if( get_sub_field('section_background') ){  // IF user/admin selected a background image to use
        echo fx_get_image_tag( get_sub_field('section_background')['url'], 'image-background', false, 'full' );
    } ?>
    <div class="container">
        <div class="row">
            <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-12">
                <h2 class="h1 testimonial-section-heading">
                    <span class="testimonial-quote">“</span>
                    <?php echo get_sub_field('section_heading'); ?>
                </h2>
                
                <!-- Slick Slider Testimonial -->
                <div class="testimonial-slick-container slick-slider" data-slick-slidesToShow="1" data-slick-autoplay="true">
                    <?php 
                    while( $theQuery->have_posts() ) : $theQuery->the_post(); ?>

                    <div class="testimonial-slick-item">
                        <div class="testimonial-content">
                            <a href="<?php echo get_the_permalink(); ?>">
                            <?php echo get_field('testimonial_content'); ?>
                            </a>
                        </div>
                        <div class="testimonial-name-and-location">
                            <a href="<?php echo get_the_permalink(); ?>">
                                <h4><?php echo get_field('client_name'); ?></h4>
                                <?php if( get_field('location') ) : ?>
                                <p><?php echo get_field('location'); ?></p>
                                <?php endif; ?>
                            </a>
                        </div>
                    </div>

                    <?php
                    endwhile;
                    wp_reset_postdata();
                    ?>
                </div>
            </div>  
        </div>
    </div>
</section>
<?php endif; ?>