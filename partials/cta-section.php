<!-- CTA Section -->
<section class="page-content pos-relative cta-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php echo fx_get_image_tag( site_url() . '/wp-content/uploads/2020/11/three-arrows-horizontal.png', 'img-responsive horizontal-arrows', false, 'full' ); ?> 
                
                <?php if( get_sub_field('cta_heading') ) : //IF there is a heading ?>
                <h2><?php echo get_sub_field('cta_heading'); ?></h2>
                <?php endif; ?>
                
                <?php 
                
                // IF there is a text content
                if( get_sub_field('cta_text') ){
                   /* Get content and sanitize */
                    $content = apply_filters('the_content', get_sub_field('cta_text') );
                    $content = str_replace(']]>', ']]&gt;', $content); 

                    echo $content;
                }
                ?>
                
                <?php if( get_sub_field('cta_button_text') && get_sub_field('cta_link') ) : // Display only if button label and link are provided ?>
                
                <div class="cta-button-container">
                    <a class="btn" href="<?php echo get_sub_field('cta_link'); ?>"><?php echo get_sub_field('cta_button_text'); ?></a>
                </div>
                
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>