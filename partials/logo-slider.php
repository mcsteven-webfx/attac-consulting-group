<div class="proudly-serve-logo-container slick-slider four" data-slick-slidesToShow="4" data-slick-autoplay="true">
    <?php
    $logos = get_field('banner_logo_slider');

    foreach( $logos as $logo ) : ?>
        <div class="proudly-serve-logo-item">
            <!-- check if there is an external link assigned -->
            <?php if( get_field('external_link', $logo['ID']) ) : ?>
                <a href="<?php echo get_field('external_link', $logo['ID']); ?>"><?php echo fx_get_image_tag( $logo['url'],'img-responsive', false, 'full' ); ?></a>
            <?php else : ?>
            <!-- check if there is no external link assigned -->
                <?php echo fx_get_image_tag( $logo['url'],'img-responsive', false, 'full' ); ?>
            <?php endif; ?>
        </div>
    <?php endforeach; ?>
</div>
