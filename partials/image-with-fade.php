<?php
// function hex2rgba($color, $opacity = false) {
//
// 	$default = 'rgb(0,0,0)';
//
// 	//Return default if no color provided
// 	if(empty($color))
//         return $default;
//
//     //Sanitize $color if "#" is provided
//     if ($color[0] == '#' ) {
//         $color = substr( $color, 1 );
//     }
//
//     //Check if color has 6 or 3 characters and get values
//     if (strlen($color) == 6) {
//         $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
//     } elseif ( strlen( $color ) == 3 ) {
//         $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
//     } else {
//         return $default;
//     }
//
//     //Convert hexadec to rgb
//     $rgb =  array_map('hexdec', $hex);
//
//     //Check if opacity is set(rgba or rgb)
//     if($opacity){
//         if(abs($opacity) > 1)
//             $opacity = 1.0;
//             $output = 'rgba('.implode(",",$rgb).','.$opacity.')';
//     } else {
//         $output = 'rgb('.implode(",",$rgb).')';
//     }
//
//     //Return rgb(a) color string
//     return $output;
// }

?>

<?php

if( get_sub_field('background_type') == 'Gradient' ){ // IF user/admin selected background as gradient
    $primary_fade_color = get_sub_field('fade_color');
    $secondary_fade_color = get_sub_field('secondary_fade_color');
    $opacity = 99; /* 60% Hex prefix */

    if( isMobile() ){ // Detect if website is being viewed on a mobile
        $bgColor = "$primary_fade_color 0, $secondary_fade_color 75%, $secondary_fade_color$opacity 100% )";
    }else{ // IF desktop
        $bgColor = "linear-gradient(90deg, $primary_fade_color 0, $secondary_fade_color 50%, $secondary_fade_color$opacity 100% )";
    }

}else{ // IF user/admin selected background as color
    $bgColor = get_sub_field('fade_color');
}
 // $bgColor = get_sub_field('fade_color');
?>

<!-- IMAGE WITH FADE -->
<section class="page-content image-with-fade pos-relative" style="background: <?php echo $bgColor; ?>;">
    <?php if( get_sub_field('image_bg') ) : // IF user/admin selected an image to use as background
        echo fx_get_image_tag( get_sub_field('image_bg')['url'] ,'image-background hidden-sm-down', false, 'full' );
    endif; ?>

    <div class="container">
        <div class="row">
            <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-7">
                <h2><?php echo get_sub_field('heading'); ?></h2>
                <div class="image-with-fade-text-container">
                    <?php
                      //  Get content and sanitize
                        $content = apply_filters('the_content', get_sub_field('text_content') );
                        $content = str_replace(']]>', ']]&gt;', $content);

                        echo $content;
                    ?>
                </div>

                <?php if( get_sub_field('button_text') || get_sub_field('button_link') ) : // Display only if button label and link are provided ?>
                <div class="image-with-fade-btn-container">
                    <a class="btn" href="<?php echo get_sub_field('button_link'); ?>"><?php echo get_sub_field('button_text'); ?></a>
                </div>
                <?php endif; ?>
            </div>
            <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-5">

            </div>
        </div>
    </div>
</section>
