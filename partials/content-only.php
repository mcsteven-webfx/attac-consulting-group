<!-- CONTENT -->
<section class="page-content pos-relative">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                
                <?php the_content(); ?>
                
            </div>
        </div>
    </div>
</section>