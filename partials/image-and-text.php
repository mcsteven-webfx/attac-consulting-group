<!--  Image with Text  -->
<section class="page-content image-and-text">
    <div class="container">
        <div class="row">
            <?php $imagePositionClass = 'right'; ?>
            <?php if( get_sub_field('image_position') == 'Left' ) : $imagePositionClass = 'left'; //check if selected image position is left ?>
            <div class="col-xxs-12 col-xs-12 col-sm-4 col-md-4">
                <div class="pos-relative the-image-container">
                    <?php if( get_sub_field('image') ) : 
                        echo fx_get_image_tag( get_sub_field('image')['url'] ,'img-responsive', false, 'full' );
                    endif; ?>
                </div>
            </div>
            <?php endif; ?>
            <div class="col-xxs-12 col-xs-12 col-sm-8 col-md-8">
                <div class="the-text-container <?php echo $imagePositionClass; ?>">
                    <h2 class="h1"><?php echo get_sub_field('heading'); ?></h2>
                    <div class="the-text-content">
                        <?php 
                        /* Get content and sanitize */
                        $content = apply_filters('the_content', get_sub_field('text_content') );
                        $content = str_replace(']]>', ']]&gt;', $content);
                        
                        echo $content;
                        ?>
                    </div>
                    
                    <?php if( get_sub_field('button_text') && get_sub_field('button_link') ) : //Display only if button label and link are provided ?>
                    <div class="image-and-text-btn-container">
                        <a class="btn" href="<?php echo get_sub_field('button_link'); ?>"><?php echo  get_sub_field('button_text'); ?></a>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php if( get_sub_field('image_position') == 'Right' ) : //check if selected image position is right ?>
            <div class="col-xxs-12 col-xs-12 col-sm-4 col-md-4">
                <div class="pos-relative the-image-container">
                    <?php if( get_sub_field('image') ) : 
                        echo fx_get_image_tag( get_sub_field('image')['url'] ,'img-responsive', false, 'full' );
                    endif; ?>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>