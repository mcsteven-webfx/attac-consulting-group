<?php
    $hamburgerMenu = '<svg xmlns="http://www.w3.org/2000/svg" width="29" height="19.333" viewBox="0 0 29 19.333">
      <path id="Icon_material-menu" data-name="Icon material-menu" d="M4.5,28.333h29V25.111H4.5Zm0-8.056h29V17.056H4.5ZM4.5,9v3.222h29V9Z" transform="translate(-4.5 -9)" fill="#242852"/>
    </svg>
    ';

    $hamburgerClose = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="29" height="29" preserveAspectRatio="xMidYMid meet" viewBox="0 0 128 128" class="iconify arty-animated" data-icon="arty-animated:16-close" style="vertical-align: -0.125em;transform: rotate(360deg);"><g stroke="currentColor" stroke-width="16" stroke-linecap="round" fill="none" fill-rule="evenodd"><path d="M8 8l112 112" class="animation-delay-0 animation-duration-6 animate-stroke stroke-length-230"></path><path d="M8 120L120 8" class="animation-delay-6 animation-duration-6 animate-stroke stroke-length-230"></path></g></svg>';

    $detect = new Mobile_Detect;
?>

<?php if( $detect->isMobile() ) : ?>
<!-- MOBILE MAIN LOGO -->
<section class="page-content pos-relative mobile-only" id="innerpage-logo">
    <div class="container">
        <h2 class="hide">Mobile Logo</h2>
        <a href="<?php echo site_url(); ?>"><?php echo fx_get_image_tag( get_field('main_logo', 'option'), 'page-header-logo', false, 'full' ); ?></a>
    </div>
</section>
<?php endif; ?>

<section class="page-content pos-relative" id="innerpage-title">
    <div class="innerpage-title-bg-holder">
        <!-- Hide the Innerpage Masthead Image -->
        <!--<?php
            // echo fx_get_image_tag( site_url() .'/wp-content/uploads/2020/11/page-title-grid-lines.png', '', false, 'full' );
        ?>-->
    </div>
    <div class="container">
        <div class="display-flex">
            <div class="page-title-container">
                <?php
                    if( is_404() ){ // IF 404 page
                        echo '<h2 class="h1">404</h2>';
                    }else if( is_search() ){ // IF Search result page
                        echo '<h2 class="h1">Search Results for: '. $_GET['s'] .'</h2>';
                    }else if( is_home() || is_singular('post') ){ // IF Blog page or single Blog Page
                        echo '<h2 class="h1">'. get_the_title( get_option('page_for_posts', true) ) .'</h2>';
                    }else if( is_singular('testimonial') ){ // IF single Testimonial Page
                        echo '<h2 class="h1">Testimonial</h2>';
                    }else if( is_archive() ){ // IF archive page
                        echo '<h2 class="h1">Archive: '. single_cat_title( '', false ) .'</h2>';
                    }else{
                        if( get_field('alternate_title') ){ // IF alternative title was provided instead of title
                            echo '<h2 class="h1">'. get_field('alternate_title') .'</h2>';
                        }else{
                            echo '<h2 class="h1">'. get_the_title() .'</h2>';
                        }
                    }
                ?>
            </div>
            <div class="page-title-space-filler"></div>
        </div>
    </div>
</section>

<?php if ( function_exists( 'yoast_breadcrumb' ) ) : ?>
<!-- Inner Page Breadcrumbs -->
<section class="page-content" id="innerpage-breadcrumbs">
    <h2 class="hide">Breadcrumbs</h2>
    <div class="container">
        <div class="row">
            <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-12">
                <div class="innerpage-breadcrumbs-container">
                    <?php
                        $crumbs = explode("/",$_SERVER["REQUEST_URI"]);
                        $count = 0;

                        foreach($crumbs as $crumb){

                            if( $count == 0 ){
                                echo '<span><a href="'. site_url() .'">Home</a></span>';
                            }else{
                                if( !empty( $crumb ) ){
                                    if( !empty( $crumbs[ $count + 1 ] ) && $crumbs[ $count + 1 ] != "" ){
                                        $args = array(
                                          'name'        => $crumb,
                                          'post_type'   => 'any',
                                          'post_status' => 'publish',
                                          'numberposts' => 1
                                        );
                                        $my_posts = get_posts($args);

                                        if( $my_posts ) :
                                          echo '<span><a href="'. get_permalink( $my_posts[0]->ID ) .'">'. $my_posts[0]->post_title .'</a></span>';
                                        endif;

                                        wp_reset_postdata();

                                    }else{

                                        $args = array(
                                          'name'        => $crumb,
                                          'post_type'   => 'any',
                                          'post_status' => 'publish',
                                          'numberposts' => 1
                                        );
                                        $my_posts = get_posts($args);

                                        if( $my_posts ) :
                                          echo '<span class="breadcrumbs-current-page">'. $my_posts[0]->post_title .'</span>';
                                        endif;

                                        wp_reset_postdata();
                                    }
                                }
                            }

                            $count++;
                        }
                    ?>
                    <?php
                        if( is_search() ){
                            echo '<span class="breadcrumbs-current-page">Search Result</span>';
                        }else if( is_archive() ){
                            echo '<span class="breadcrumbs-current-page">'. single_cat_title( '', false ) .'</span>';
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
