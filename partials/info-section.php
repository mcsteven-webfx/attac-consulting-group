<!-- WYSIWYG SECTION -->
<section class="page-content pos-relative info-section" style="background-color: <?php echo get_sub_field('wysiwyg_background'); ?>">
    <div class="container">
        <div class="row">
            <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-12">                  
                <?php 
                if( get_sub_field('info_text') ){ // IF there are info text provided
                   /* Get content and sanitize */
                    $content = apply_filters('the_content', get_sub_field('info_text') );
                    $content = str_replace(']]>', ']]&gt;', $content); 

                    echo $content;
                }
                ?>
                
                <?php if( get_sub_field('add_testimonial') ) : ?>
                
                <div class="info-section-testimony-container">
                    <?php
                        $args = array(
                            'p' => get_sub_field('testimony'),
                            'post_type' => 'testimonial',
                            'post_status'   => 'publish',
                            'posts_per_page' => -1,
                        );

                        $theQuery = new WP_Query( $args );
                    
                        // IF there are items to display
                        if( $theQuery->have_posts() ){
                             $data .= '<div class="testimony-container pos-relative">';
                             
                             $data .= fx_get_image_tag( site_url() .'/wp-content/uploads/2020/11/three-arrows-vertical.png','img-responsive', false, 'full' );
                             
                             while( $theQuery->have_posts() ) : $theQuery->the_post();
                                $theClass = 'none';
                             
                                $data .= '<div class="testimony-content">';
                                    /* Get content and sanitize */
                                    $content = apply_filters('the_content', get_field('testimonial_content') );
                                    $content = str_replace(']]>', ']]&gt;', $content);
                                    
                                    $data .= $content;
                                $data .= '</div>';
                             
                                $data .= '<h4 class="testimony-author">'. get_field('client_name') .'</h4>';
                                
                                if( get_field('job_position') ){ // IF job position are provided
                                    $data .= '<p class="testimony-job-position">'. get_field('job_position') .'</p>';
                                    $theClass = 'has-position';
                                }
                             
                                if( get_field('location') ){ // IF job location are provided
                                    $data .= '<p class="testimony-location '. $theClass .'">'. get_field('location') .'</p>';
                                }
                             
                             endwhile;
                             wp_reset_postdata();
                             
                             $data .= '</div>';
                             
                             echo $data;
                        }
                    ?>
                </div>
                
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>