<?php     
    $hasImage = false;
    $buttonText = 'Read More';

    if( get_field('post_hero_image') ){
        $hasImage = true;
    }

    if( get_post_type() == 'page' ){
        $buttonText = 'View Page';
    }
?>

<div class="col-xxs-12 col-xs-12 col-sm-12 col-md-12 soft-bottom soft-xs-bottom soft-triple-sm-bottom soft-triple-md-bottom">
    <?php if( $hasImage ) : // IF user/admin provided another image to use on banner ?>
    <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-4">
        <div class="pos-relative the-image-container">
            <?php echo fx_get_image_tag( get_field('post_hero_image')['url'],'image-background', false, 'full' ); ?>
        </div>
    </div>
    <?php endif; ?>
    
    <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-<?php if( $hasImage ) : echo '8 has-post-image'; else : echo '12 no-post-image'; endif; ?>">
        <div class="the-text-container left soft-sm-top soft-bottom hard-md-top">
            <h2 class="h1"><?php echo get_the_title(); ?></h2>
            <p class="post-item__meta"><?php echo get_the_date(); ?></p>
            <div class="the-text-content">
                <?php 
                /* Get content and sanitize */
                $content = apply_filters('the_content', wp_trim_words( get_the_content(), 15 ) );
                $content = str_replace(']]>', ']]&gt;', $content);

                echo $content;
                ?>
            </div>

            <div class="image-and-text-btn-container">
                <a class="btn" href="<?php echo get_the_permalink(); ?>"><?php echo $buttonText; ?></a>
            </div>
        </div>
    </div>
</div>