<!-- Image Buttons -->
<section class="page-content" id="help-you-solve">
    <div class="container">
        <div class="row">
            <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-12">
                <?php echo fx_get_image_tag( site_url() . '/wp-content/uploads/2020/11/three-arrows-horizontal.png', 'img-responsive horizontal-arrows', false, 'full' ); ?>
                <?php if( get_sub_field('section_subheading') ) : // IF section subheading are provided ?>
                    <p class="service-subheading"><?php echo get_sub_field('section_subheading'); ?></p>
                <?php endif; ?>
                <?php if( get_sub_field('section_heading') ) : // IF section headings are provided ?>
                    <h2 class="h1"><?php echo get_sub_field('section_heading'); ?></h2>
                <?php endif; ?>
                <?php if( get_sub_field('text_description') ) : // IF section descriptions are provided ?>
                    <div class="service-section-description"><?php echo get_sub_field('text_description'); ?></div>
                <?php endif; ?>
            </div>
            <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-12">
                <div class="feat-services-container row">
                    <?php 
                        $colCount = count( get_sub_field('image_button_repeater') ); // get total item count
                    
                        if( $colCount >= 4 ){
                            $imageCount = 3;
                        }else{
                            if( $colCount == 3 ){
                                $imageCount = 4;
                            }else if( $colCount == 2 ){
                                $imageCount = 6;
                            }else{
                                $imageCount = 12;
                            }
                        }
                        
                        // IF there are items created
                        if( have_rows('image_button_repeater') ){
                            $data = '';
                            while( have_rows('image_button_repeater') ) : the_row();                            
                                $field = get_sub_field('image_button_group');
                            
                                $data .= '<div class="col-xxs-12 col-xs-12 col-sm-'.$imageCount.' col-md-'.$imageCount.'">';
                                    $data .= '<div class="service-data-container pos-relative col-count-'. $colCount .'">';
                                        $data .= fx_get_image_tag( $field['image']['url'],'img-responsive', false, 'full' );   

                                        $data .= '<div class="service-hovered">';
                                            $data .= '<h3>'. $field['image_heading'] .'</h3>';

                                            /* Get content and sanitize */
                                            $content = apply_filters('the_content', wp_trim_words( $field['image_text_content'], 15 ) );
                                            $content = str_replace(']]>', ']]&gt;', $content);

                                            $data .= $content;

                                            $data .= '<a class="btn-tertiary learn-more" href="'. $field['image_link'] .'"><span>Learn More</span> <span class="icon-Arrow"></span></a>';
                                        $data .= '</div>';
                                    $data .= '</div>';
                                $data .= '</div>';
                            endwhile;
                            
                            echo $data;
                        }
                    ?>
                </div>
            </div>  
        </div>
    </div>
</section>