<!-- TEAM SECTION -->
<section class="page-content team-section" id="<?php echo get_sub_field('category')[0]->slug; ?>" style="background-color: <?php echo get_sub_field('background'); ?>;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php echo fx_get_image_tag( site_url() . '/wp-content/uploads/2020/11/three-arrows-horizontal.png', 'img-responsive horizontal-arrows', false, 'full' ); ?>
                <h2 class="h1"><?php echo get_sub_field('heading'); ?></h2>
            </div>

            <div class="leadership-team-container display-flex row">
                <?php 
                    if( get_sub_field('category') ){ // IF user/admin selected a category 
                        $args = array(
                            'post_type' => 'our_team',
                            'post_status'   => 'publish',
                            'posts_per_page' => -1,
                            'order' => 'ASC',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'team_category',
                                    'field'    => 'term_id',
                                    'terms'    => get_sub_field('category')[0]->term_taxonomy_id,
                                ),
                            ),
                        );
                    }else{ // IF user/admin did not select any category 
                        $args = array(
                            'post_type' => 'our_team',
                            'post_status'   => 'publish',
                            'posts_per_page' => -1,
                            'order' => 'ASC'
                        );
                    }
                    

                    $theQuery = new WP_Query( $args );

                    if( $theQuery->have_posts() ) {
                        while( $theQuery->have_posts() ) : $theQuery->the_post(); ?>

                        <div class="team-member-container">
                            <div class="team-member pos-relative">
                                <?php if( get_sub_field('show_image') ) : // IF user/admin enabled to show image ?>
                                <div class="pos-relative img-bio-container">
                                    <?php echo fx_get_image_tag( get_the_post_thumbnail_url(), 'img-responsive', false, 'full' ); ?>
                                    
                                    <div class="team-member-overlay-bio">
                                        <a class="btn-tertiary"><span>View Full Bio</span> <span class="icon-Arrow"></span></a>
                                    </div>
                                </div>
                                <?php endif; ?>
                                <div class="team-member-details pos-relative">
                                    <h4><?php echo get_the_title(); ?></h4>
                                    <p class="team-position"><?php echo get_field('position'); ?></p>
                                    <?php if( get_field('department') ) : ?>
                                        <p class="team-department"><?php echo get_field('department'); ?></p>
                                    <?php endif; ?>
                                    <?php if( get_field('linkedin-profile') ) : ?>
                                        <a class="linkedin-profile" href="<?php echo get_field('linkedin-profile'); ?>">
                                            <span class="icon-Linkedin"></span>
                                        </a>
                                    <?php endif; ?>
                                    <?php if( get_field('email-link') ) : ?>
                                        <a class="email-link" href="mailto:<?php echo get_field('email-link'); ?>">
                                            <span class="icon-Email"></span>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="team-member-bio-container">
                                <h4><?php echo get_the_title(); ?></h4>
                                <div class="team-position-container">
                                    <span class="team-position"><?php echo get_field('position'); ?></span> 
                                    <?php if( get_field('department') ) : // IF team department are provided ?>
                                        <span class="team-department"><?php echo get_field('department'); ?></span>
                                    <?php endif; ?>
                                </div>

                                <?php if( get_the_content() ) : // IF user/admin provided content for this item ?>
                                <div class="team-description-container">
                                    <?php 
                                        // Get content and sanitize
                                        $content = apply_filters('the_content', get_the_content() );
                                        $content = str_replace(']]>', ']]&gt;', $content);

                                        echo $content;
                                    ?>
                                </div>
                                <?php endif; ?>
                                
                                <?php if( !get_sub_field('show_image') ) : // IF user/admin disasbled to show image ?>
                                <div class="team-member-close-bio-container">
                                    <div class="team-member-close-bio-btn">Close Bio <span class="icon-Arrow icon-Arrow-caret"></span></div>
                                </div>
                                <?php endif; ?>
                                
                            </div>
                        </div>

                <?php   endwhile;
                        wp_reset_postdata();
                    }
                ?>
            </div>
        </div>
    </div>
</section>