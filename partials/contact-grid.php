<!-- CONTACT GRID SECTION -->
<section class="page-content pos-relative" id="contact-grid">


    <div class="container">
        <div class="row">
            <div class="col-md-12">
                                  <h3>Contact Us</h3>
                <!--Start Contact Card-->
            <?php if( have_rows('contact_details', 'option') ):
                while( have_rows('contact_details', 'option') ) : the_row(); ?>
                <div class="col-md-6">
                    <div class="contact-card">
                    <?php if( get_sub_field('contact_grid_name', 'option') ) :  ?>
                        <h5 class="contact-name">
                            <?php echo get_sub_field('contact_grid_name', 'option'); ?>
                        </h5>
                    <?php endif; ?>

                    <?php if( get_sub_field('contact_position', 'option') ) :  ?>
                        <p class="contact-position">
                            <?php echo get_sub_field('contact_position', 'option'); ?>
                        </p>
                    <?php endif; ?>

                    <?php if( get_sub_field('contact_email', 'option') ) :  ?>
                        <a class="email-link" href="mailto:<?php echo get_sub_field('contact_email', 'option'); ?>">
                            <span class="icon-Email"></span></a>
                    <?php endif; ?>

                    <?php if( get_sub_field('contact_linkedin_profile', 'option') ) :  ?>
                        <a class="linkedin-profile" href="<?php echo get_sub_field('contact_linkedin_profile', 'option'); ?>">
                        <span class="icon-Linkedin"></span>
                    </a>
                    <?php endif; ?>
                    </div>
                </div>
                <?php endwhile;?>
            <?php endif; ?>
            <!--End Contact Card-->
            </div>
        </div>

    </div>

</section>
