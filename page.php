<!--  Inner Page Title Row  -->
<?php get_template_part( 'partials/inner-page-title' ); ?>    

<?php 

/* Check if there are contents */
require_once get_template_directory() . '/layouts/flexible-content.php';

?>

<?php if( get_field('enable_contact_grid') ) { ?>
        <?php get_template_part( 'partials/contact-grid' ); ?>
<?php } ?>

