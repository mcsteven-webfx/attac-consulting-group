<?php
    $twitter = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
      <path id="twitter_2_" data-name="twitter (2)" d="M0,0V100H100V0ZM77.528,35.881q.041.914.041,1.837c0,18.77-14.286,40.412-40.412,40.412a40.2,40.2,0,0,1-21.773-6.382,28.666,28.666,0,0,0,21.028-5.882A14.218,14.218,0,0,1,23.144,56a14.215,14.215,0,0,0,6.415-.243A14.213,14.213,0,0,1,18.165,41.834c0-.06,0-.12,0-.179A14.146,14.146,0,0,0,24.6,43.432,14.219,14.219,0,0,1,20.2,24.469,40.324,40.324,0,0,0,49.48,39.309a14.212,14.212,0,0,1,24.2-12.955A28.429,28.429,0,0,0,82.7,22.908a14.244,14.244,0,0,1-6.245,7.858,28.4,28.4,0,0,0,8.158-2.237A28.842,28.842,0,0,1,77.528,35.881Z" fill-rule="evenodd"/>
    </svg>
    ';

    $linkedIn = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
      <path id="Path_2" data-name="Path 2" d="M0,0V100H100V0ZM31.1,83.077H16.267V38.441H31.1ZM23.685,32.349h-.1c-4.977,0-8.2-3.429-8.2-7.715,0-4.38,3.32-7.711,8.4-7.711s8.2,3.332,8.3,7.711C32.08,28.92,28.862,32.349,23.685,32.349Zm60.93,50.729H69.775V59.2c0-6-2.144-10.092-7.515-10.092a8.115,8.115,0,0,0-7.613,5.427,10.168,10.168,0,0,0-.492,3.617V83.078H39.311s.2-40.449,0-44.636H54.154v6.325a14.738,14.738,0,0,1,13.375-7.373c9.766,0,17.087,6.378,17.087,20.087Z" fill-rule="evenodd"/>
    </svg>
    ';

    $googleMap = '<svg id="google-maps_1_" data-name="google-maps (1)" xmlns="http://www.w3.org/2000/svg" width="23.228" height="31.799" viewBox="0 0 23.228 31.799">
      <path id="Path_3" data-name="Path 3" d="M80.614,0A11.617,11.617,0,0,0,69,11.62c0,7.065,4.841,8.73,7.38,13.061,2.364,4.031,2.366,7.117,4.234,7.117,1.936,0,1.814-2.875,4.234-7.117s7.38-6,7.38-13.061A11.617,11.617,0,0,0,80.614,0Zm0,16.073a4.347,4.347,0,1,1,4.344-4.347A4.346,4.346,0,0,1,80.614,16.073Z" transform="translate(-69 0)" fill="#4caf50"/>
      <g id="Group_37" data-name="Group 37" transform="translate(10.654 0)">
        <path id="Path_4" data-name="Path 4" d="M241.5,0c-.323,0-.643.014-.96.04A11.619,11.619,0,0,1,251.2,11.62c0,7.065-4.962,8.819-7.38,13.061-1.916,3.36-2.238,5.862-3.251,6.757a1.339,1.339,0,0,0,.938.36c1.936,0,1.814-2.875,4.234-7.117s7.38-6,7.38-13.061A11.617,11.617,0,0,0,241.5,0Z" transform="translate(-240.545)" fill="#43a047"/>
      </g>
      <path id="Path_5" data-name="Path 5" d="M77.543,60.856,69.907,68.5A11.382,11.382,0,0,1,69,63.824,11.586,11.586,0,0,1,72.35,55.66Z" transform="translate(-69.001 -52.203)" fill="#f44336"/>
      <path id="Path_6" data-name="Path 6" d="M212.709,29.209a4.346,4.346,0,0,0-6.175-6.117l7.692-7.7a11.655,11.655,0,0,1,6.1,6.191l-7.619,7.623Z" transform="translate(-197.992 -14.44)" fill="#42a5f5"/>
      <path id="Path_7" data-name="Path 7" d="M320.592,15.4l-.648.649a11.643,11.643,0,0,1,5.321,6.973l1.43-1.431A11.655,11.655,0,0,0,320.592,15.4Z" transform="translate(-304.359 -14.44)" fill="#2196f3"/>
      <path id="Path_8" data-name="Path 8" d="M97.4,145.433l-8.386,8.391c-1.823-2.3-4.163-3.988-5.425-6.868l7.636-7.64a4.346,4.346,0,1,0,6.175,6.117Z" transform="translate(-82.682 -130.663)" fill="#ffc107"/>
      <path id="Path_9" data-name="Path 9" d="M135.81.956l-7.692,7.7-5.193-5.2A11.618,11.618,0,0,1,135.81.956Z" transform="translate(-119.576 0)" fill="#1e88e5"/>
      <path id="Path_10" data-name="Path 10" d="M241.506,0q-.485,0-.96.04h0A11.539,11.539,0,0,1,245.477,1.6l.648-.649A11.564,11.564,0,0,0,241.506,0Z" transform="translate(-229.892 0)" fill="#1976d2"/>
    </svg>
    ';

     $hamburgerMenu = '<svg xmlns="http://www.w3.org/2000/svg" width="29" height="19.333" viewBox="0 0 29 19.333">
      <path id="Icon_material-menu" data-name="Icon material-menu" d="M4.5,28.333h29V25.111H4.5Zm0-8.056h29V17.056H4.5ZM4.5,9v3.222h29V9Z" transform="translate(-4.5 -9)" fill="#242852"/>
    </svg>
    ';

     $hamburgerClose = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="29" height="29" preserveAspectRatio="xMidYMid meet" viewBox="0 0 128 128" class="iconify arty-animated" data-icon="arty-animated:16-close" style="vertical-align: -0.125em;transform: rotate(360deg);"><g stroke="currentColor" stroke-width="16" stroke-linecap="round" fill="none" fill-rule="evenodd"><path d="M8 8l112 112" class="animation-delay-0 animation-duration-6 animate-stroke stroke-length-230"></path><path d="M8 120L120 8" class="animation-delay-6 animation-duration-6 animate-stroke stroke-length-230"></path></g></svg>';

    $detect2 = new Mobile_Detect;
?>

        <footer class="page-footer" id="page-footer">
            <div class="container">
                <?php if( !$detect2->isTablet() ) : ?>
                <div class="row desktop-mobile-only">
                    <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-3">
                        <!--  Back To Top Icon area  -->
                        <a href="<?php echo site_url(); ?>"><?php echo fx_get_image_tag( site_url() .'/wp-content/uploads/2020/11/attac-logo-white-footer-transparent.png','footer-logo img-responsive', false, 'full', array('alt' => 'ATTAC Consulting Group') ); ?> </a>

                        <div id="back-to-top" class="back-to-top desktop-only">
                            <a class="btn">
                                <span>Back to top </span><span class="icon-Back-to-Top"></span>
                            </a>
                        </div>
                    </div>
                    <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-2">
                        <?php
                        // Output the footer navigation
                        wp_nav_menu(
                            array(
                                'menu'            => 8,
                                'container'       => 'nav',
                                'container_class' => 'footer-navigation',
                                'depth'           => 1
                            )
                        );
                        ?>
                    </div>
                    <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-3 footer-contact-us">
                        <?php if( $detect2->isMobile() ) : ?>
                        <div class="col-xxs-12 col-xs-12 col-sm-12 footer-social-medias mobile-only">
                            <h4 class="footer-heading">Follow Us</h4>
                            <div class="row">
                                <!-- TWITTER  -->
                                <?php if( get_field('twitter', 'option') ) : ?>
                                <div class="col-xxs-3 col-xs-3 col-sm-3 col-md-4">
                                    <a href="<?php echo get_field('twitter', 'option'); ?>"><span class="icon-Twitter"></span></a>
                                </div>
                                <?php endif; ?>

                                <!-- LINKEDIN  -->
                                <?php if( get_field('linkedin', 'option') ) : ?>
                                <div class="col-xxs-3 col-xs-3 col-sm-3 col-md-4">
                                    <a href="<?php echo get_field('linkedin', 'option'); ?>"><span class="icon-Linkedin"></span></a>
                                </div>
                                <?php endif; ?>

                                <!-- GOOGLE MAP  -->
                                <?php if( get_field('google_map', 'option') ) : ?>
                                <div class="col-xxs-3 col-xs-3 col-sm-3 col-md-4">
                                    <a href="<?php echo get_field('google_map', 'option'); ?>"><?php echo $googleMap; ?></a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php endif; ?>

                        <h4 class="footer-heading"><a style="color: #fff;" href="/contact">Contact Us</a></h4>
                        <?php if( get_field('phone', 'option') ) :
                            $phone = preg_replace( '/[^A-Za-z0-9\-]/', '', str_replace(' ', '', get_field('phone', 'option') ) );
                        ?>
                        <p><a href="tel:<?php echo $phone; ?>"><?php echo get_field('phone', 'option'); ?></a></p>

                        <?php endif; ?>
                        <?php if( get_field('email', 'option') ) : ?>
                        <p><a href="mailto:<?php echo get_field('email', 'option'); ?>"><?php echo get_field('email', 'option'); ?></a></p>
                        <?php endif; ?>

                        <?php if( get_field('link_url', 'option') ) : ?>
                        <p><a href="<?php echo get_field('link_url', 'option'); ?>"><?php echo get_field('link_label', 'option'); ?></a></p>
                        <?php endif; ?>

                    </div>

                    <?php if( !$detect2->isMobile() && !$detect2->isTablet() ) : ?>
                    <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-4 desktop-only">
                        <div class="row">
                            <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-8 footer-our-location">
                                <h4 class="footer-heading">Our Location</h4>
                                <?php if( get_field('address', 'option') ) : ?>
                                <p><?php echo get_field('address', 'option'); ?></p>
                                <?php endif; ?>

                                <?php if( get_field('direction_link', 'option') ) : ?>
                                <a class="btn-tertiary" href="<?php echo get_field('direction_link', 'option'); ?>"><span>Get Directions</span><span class="icon-Arrow"></span></a>
                                <?php endif; ?>
                            </div>
                            <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-4 footer-social-medias">
                                <h4 class="footer-heading">Follow Us</h4>
                                <div class="row">
                                    <!-- TWITTER  -->
                                    <?php if( get_field('twitter', 'option') ) : ?>
                                    <div class="col-xxs-3 col-xs-3 col-sm-3 col-md-4">
                                        <a href="<?php echo get_field('twitter', 'option'); ?>"><span class="icon-Twitter"></span></a>
                                    </div>
                                    <?php endif; ?>

                                    <!-- LINKEDIN  -->
                                    <?php if( get_field('linkedin', 'option') ) : ?>
                                    <div class="col-xxs-3 col-xs-3 col-sm-3 col-md-4">
                                        <a href="<?php echo get_field('linkedin', 'option'); ?>"><span class="icon-Linkedin"></span></a>
                                    </div>
                                    <?php endif; ?>

                                    <!-- GOOGLE MAP  -->
                                    <?php if( get_field('google_map', 'option') ) : ?>
                                    <div class="col-xxs-3 col-xs-3 col-sm-3 col-md-4">
                                        <a href="<?php echo get_field('google_map', 'option'); ?>"><?php echo $googleMap; ?></a>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>

                </div>
                <?php endif; ?>
                <?php if( $detect2->isTablet() ) : ?>
                <div class="row tablet-only">
                    <div class="col-xxs-12 col-xs-12 col-sm-6 col-md-5">
                        <!--  Back To Top Icon area  -->
                        <a href="<?php echo site_url(); ?>"><?php echo fx_get_image_tag( site_url() .'/wp-content/uploads/2020/11/attac-logo-white-footer-transparent.png','footer-logo img-responsive', false, 'full', array('alt' => 'ATTAC Consulting Group') ); ?> </a>

                        <div id="back-to-top" class="back-to-top desktop-only tablet-only">
                            <a class="btn" >
                                <span>Back to top </span><span class="icon-Back-to-Top"></span>
                            </a>
                        </div>
                    </div>

                    <!--  TABLET OTHER DETAILS  -->
                    <div class="col-xxs-12 col-xs-12 col-sm-6 col-md-7">
                        <div class="footer-our-location">
                            <h4 class="footer-heading">Our Location</h4>
                            <?php if( get_field('address', 'option') ) : ?>
                            <p><?php echo get_field('address', 'option'); ?></p>
                            <?php endif; ?>

                            <?php if( get_field('direction_link', 'option') ) : ?>
                            <a class="btn-tertiary" href="<?php echo get_field('direction_link', 'option'); ?>"><span>Get Directions</span> <span><svg xmlns="http://www.w3.org/2000/svg" width="61" height="100" viewBox="0 0 61 100"><path id="Polygon_1" data-name="Polygon 1" d="M50,0l50,61H0Z" transform="translate(61 0) rotate(90)"></path></svg></span></a>
                            <?php endif; ?>

                            <div class="footer-contact-us">
                                <h4 class="footer-heading">Contact Us</h4>
                                <?php if( get_field('phone', 'option') ) :
                                    $phone = preg_replace( '/[^A-Za-z0-9\-]/', '', str_replace(' ', '', get_field('phone', 'option') ) );
                                ?>
                                <p><a href="tel:<?php echo $phone; ?>"><?php echo get_field('phone', 'option'); ?></a></p>
                                <?php endif; ?>
                                <?php if( get_field('email', 'option') ) : ?>
                                <p><a href="mailto:<?php echo get_field('email', 'option'); ?>"><?php echo get_field('email', 'option'); ?></a></p>
                                <?php endif; ?>
                            </div>

                            <div class="footer-social-medias">
                                <!-- TWITTER  -->
                                <?php if( get_field('twitter', 'option') ) : ?>
                                <div class="col-xxs-3 col-xs-3 col-sm-3 col-md-4">
                                    <a href="<?php echo get_field('twitter', 'option'); ?>"><span class="icon-Twitter"></span></a>
                                </div>
                                <?php endif; ?>

                                <!-- LINKEDIN  -->
                                <?php if( get_field('linkedin', 'option') ) : ?>
                                <div class="col-xxs-3 col-xs-3 col-sm-3 col-md-4">
                                    <a href="<?php echo get_field('linkedin', 'option'); ?>"><span class="icon-Linkedin"></span></a>
                                </div>
                                <?php endif; ?>

                                <!-- GOOGLE MAP  -->
                                <?php if( get_field('google_map', 'option') ) : ?>
                                <div class="tablet-col">
                                    <a href="<?php echo get_field('google_map', 'option'); ?>"><?php echo $googleMap; ?></a>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>

                    <!--  TABLET FOOTER MENU  -->
                    <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-12">
                        <?php
                        // Output the footer navigation
                        wp_nav_menu(
                            array(
                                'menu'            => 'footer-menu',
                                'container'       => 'nav',
                                'container_class' => 'footer-navigation',
                                'depth'           => 1
                            )
                        );
                        ?>
                    </div>
                </div>
                <?php endif; ?>

                <?php if( $detect2->isMobile() ) : ?>
                <div id="back-to-top" class="back-to-top mobile-only">
                    <a class="btn" >
                        <span>Back to top </span><span class="icon-Back-to-Top"></span>
                    </a>
                </div>
                <?php endif; ?>
            </div>
            <div class="footer-copyright-container">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="footer-copright-links">
                                <li>
                                    <a class="sitemap" href="<?php echo site_url(); ?>/sitemap/"><p>Site Map</p></a>
                                </li>
                                <li>
                                    <a class="privacy-policy"><p>Privacy Policy</p></a>
                                </li>
                                <li>
                                    <a class="site-credits"><p>Site Credits</p></a>
                                </li>
                                <li><p>Copyright © <?php echo date('Y'); ?>. All Rights Reserved</p></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>


        <div class="mobile-main-navigation-container hidden-sm-up" id="mobile-main-navigation">
<!--             OVERLAY MENU -->
            <!-- <div class="mobile-overlay-menu-container">
                <?php// echo wp_nav_menu( array( 'menu' => 2 ) ); ?>
            </div> -->

<!--             SEARCH FORM  -->
            <div class="header-search-form-container">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <?php echo get_search_form(); ?>
                        </div>
                    </div>
                </div>
            </div>

<!--             STICKY NAVIGATION  -->
            <div class="container hard-sides">
                <div class="display-flex">
                    <div class="mobile-main-cta-btn-container">
                        <a class="btn" href="<?php echo get_field('cta_link', 'option'); ?>"><?php echo get_field('cta_text', 'option'); ?></a>
                    </div>
                    <div class="mobile-main-nav-search-btn-container">
                        <a class="mobile-nav-search-btn-trigger"><span class="icon-Search"></span></a>
                    </div>
                    <div class="mobile-main-menu-toggle-container">
			<?php ob_start(); ?>
                        <?php ubermenu( 'main' , array( 'menu' => 2 ) ); ?>
			<?php echo str_replace( 'ubermenu-main-2', 'ubermenu-main-2-1', ob_get_clean() ); ?>
                    </div>
                </div>
            </div>
        </div>

        <!--<div class="testing-links">
            <a href="<?php //the_field( 'page_url' ); ?>">page url</a>
            <a href="<?php //the_field( 'page_link' ); ?>">link</a>
        </div>-->

        <div class="wp-footer-scripts-container">
            <?php wp_footer(); ?>
        </div>

        <!-- Extra Check to make sure jquery gets included -->
        <script>window.jQuery || document.write('<script src="<?php echo get_template_directory_uri(); ?>/assets/js/vendor/jquery.min.js"><\/script>')</script>     <script src="//cdn.leadmanagerfx.com/js/mcfx/4062"></script>
        <script src="//cdn.leadmanagerfx.com/phone/js/4062"></script>
    </body>
</html>
