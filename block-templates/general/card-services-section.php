<!-- Card Services Section -->
<section class="page-content pos-relative card-services-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <?php if( get_field('heading') ) :  ?>
                    <div class="card-heading">
                        <h2><?php echo get_field('heading'); ?></h2>
                    </div>
                <?php endif; ?>

                <?php if( have_rows('cards') ): ?>
                    <div class="card-services">
                        <?php while( have_rows('cards') ): the_row();
                                $title = get_sub_field('title');
                                $titlelink = get_sub_field('link');
                            ?>
                            <div class="card-services-container">
                            <?php if( have_rows('content') ): ?>
                                <div class="card-text-content">
                                    <div class="card-title">
                                        <h4><a href="<?php echo $titlelink; ?>"><?php echo $title; ?></a></h4>
                                    </div>
                                    <ul>
                                        <?php while( have_rows('content') ): the_row(); ?>
                                            <li class="card-text">
                                                <?php
                                                    $text = get_sub_field('text');
                                                    $link = get_sub_field('link');
                                                    if ($link) {
                                                      $text = '<a class="card-link" href="'.$link.'">'.$text.'</a>';
                                                    }
                                                    echo $text;
                                                ?>
                                            </li>
                                        <?php endwhile; ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>

            </div>
        </div>
    </div>
</section>
<!-- Card Services Section -->
