<!-- Image With Fade Section -->
<?php

if( get_field('background_type') == 'Gradient' ){ // IF user/admin selected background as gradient
    $primary_fade_color = get_field('fade_color');
    $secondary_fade_color = get_field('secondary_fade_color');
    $opacity = 99; /* 60% Hex prefix */

    if( isMobile() ){ // Detect if website is being viewed on a mobile
        $bgColor = "$primary_fade_color 0, $secondary_fade_color 75%, $secondary_fade_color$opacity 100% )";
    }else{ // IF desktop
        $bgColor = "linear-gradient(90deg, $primary_fade_color 0, $secondary_fade_color 50%, $secondary_fade_color$opacity 100% )";
    }

}else{ // IF user/admin selected background as color
    $bgColor = get_field('fade_color');
}
 // $bgColor = get_field('fade_color');
?>

<!-- IMAGE WITH FADE -->
<section class="page-content image-with-fade pos-relative" style="background: <?php echo $bgColor; ?>;">
    <?php if( get_field('background_image') ) : // IF user/admin selected an image to use as background
        echo fx_get_image_tag( get_field('background_image')['url'] ,'image-background hidden-sm-down', false, 'full' );
    endif; ?>

    <div class="container">
        <div class="row">
            <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-7">
                <h2><?php echo get_field('heading'); ?></h2>
                <div class="image-with-fade-text-container">
                    <?php
                      //  Get content and sanitize
                        $content = apply_filters('the_content', get_field('wysiwyg') );
                        $content = str_replace(']]>', ']]&gt;', $content);

                        echo $content;
                    ?>
                </div>

                <?php if( get_field('button_text') || get_field('button_link') ) : // Display only if button label and link are provided ?>
                <div class="image-with-fade-btn-container">
                    <a class="btn" href="<?php echo get_field('button_link'); ?>"><?php echo get_field('button_text'); ?></a>
                </div>
                <?php endif; ?>
            </div>
            <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-5">

            </div>
        </div>
    </div>
</section>
<!-- Image With Fade Section -->
