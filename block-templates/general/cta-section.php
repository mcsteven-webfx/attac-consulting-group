<!-- CTA Section -->
<section class="page-content pos-relative cta-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php echo fx_get_image_tag( site_url() . '/wp-content/uploads/2020/11/three-arrows-horizontal.png', 'img-responsive horizontal-arrows', false, 'full' ); ?>

                <?php if( get_field('heading') ) : //IF there is a heading ?>
                <h2><?php echo get_field('heading'); ?></h2>
                <?php endif; ?>

                <?php

                // IF there is a text content
                if( get_field('text') ){
                   /* Get content and sanitize */
                    $content = apply_filters('the_content', get_field('text') );
                    $content = str_replace(']]>', ']]&gt;', $content);

                    echo $content;
                }
                ?>

                <?php if( get_field('button_text') && get_field('link') ) : // Display only if button label and link are provided ?>

                <div class="cta-button-container">
                    <a class="btn" href="<?php echo get_field('link'); ?>"><?php echo get_field('button_text'); ?></a>
                </div>

                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<!-- CTA Section -->
