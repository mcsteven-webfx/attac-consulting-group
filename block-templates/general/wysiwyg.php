<!-- WYSIWYG Section -->
<section class="page-content pos-relative info-section" style="background-color: <?php echo get_field('background'); ?>">
    <div class="container">
        <div class="row">
            <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-12">
                <?php
                if( get_field('wysiwyg') ){ // IF there are info text provided
                   /* Get content and sanitize */
                    $content = apply_filters('the_content', get_field('wysiwyg') );
                    $content = str_replace(']]>', ']]&gt;', $content);

                    echo $content;
                }
                ?>
            </div>
        </div>
    </div>
</section>
<!-- WYSIWYG Section -->
