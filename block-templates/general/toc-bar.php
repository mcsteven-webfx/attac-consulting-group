<!-- TOC Bar Section -->
<section class="page-content pos-relative toc-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <?php if( have_rows('toc_bar_container') ): ?>
                    <div class="toc-link-container">
                        <?php while( have_rows('toc_bar_container') ): the_row();
                            $label = get_sub_field('label');
                            $link = get_sub_field('link');
                        ?>
                            <a class="toc-link" href="<?php echo $link; ?>">
                                <?php echo $label; ?>
                            </a>
                        <?php endwhile; ?>
                    </div>
                <?php endif; ?>

            </div>
        </div>
    </div>
</section>
<!-- TOC Bar Section -->
