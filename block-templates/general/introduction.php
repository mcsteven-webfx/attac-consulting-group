<!-- Introduction Text Image Section -->
<section class="page-content pos-relative" id="innerpage-banner">
    <h2 class="hide">Page Banner</h2>
    <?php echo fx_get_image_tag( get_field('image')['url'], 'image-background img-desktop-only', false, 'full' ); ?>
    <?php echo fx_get_image_tag( get_field('image')['url'], 'image-background img-mobile-only', false, 'full' ); ?>
    <div class="container">
        <div class="row">
            <div class="col-xxs-12 col-xs-12 col-sm-4 col-md-4">
                <?php echo fx_get_image_tag( get_field('image')['url'], 'img-responsive img-tablet-only', false, 'full' ); ?>
            </div>
            <div class="col-xxs-12 col-xs-12 col-sm-8 col-md-8">
                <div class="innerpage-banner-content-container">
                    <?php if( get_field('show_page_title') ) : // IF user/admin enabled to show page title ?>

                      <?php if( get_field( 'post_subheading', get_the_ID() ) ) : $hasSubheading = ''; ?>
                      <?php else : $hasSubheading = 'no-subheading'; ?>
                      <?php endif; ?>

                      <div class="pos-relative innerpage-banner-heading-container <?php echo $hasSubheading; ?>">
                          <?php
                              echo fx_get_image_tag( site_url() .'/wp-content/uploads/2020/11/three-arrows-horizontal.png','img-responsive img-mobile-only', false, 'full' );

                              echo fx_get_image_tag( site_url() .'/wp-content/uploads/2020/11/three-arrows-vertical.png', 'img-responsive img-desktop-tablet-only', false, 'full' );
                          ?>
                          <?php if( get_field( 'post_subheading', get_the_ID() ) ) :  ?>
                          <p><?php echo get_field( 'post_subheading', get_the_ID() ); ?></p>
                              <?php endif; ?>

                          <h2>
                          <?php if( get_field( 'alternate_title', get_the_ID() ) ) : // IF user/admin provided an alternative title
                              echo get_field( 'alternate_title', get_the_ID() );
                          else :
                              echo get_the_title();
                          endif;
                          ?>
                          </h2>
                      </div>
                      <?php else: ?>
                        <div class="pos-relative innerpage-banner-heading-container <?php echo $hasSubheading; ?>">
                            <?php
                                echo fx_get_image_tag( site_url() .'/wp-content/uploads/2020/11/three-arrows-horizontal.png','img-responsive img-mobile-only', false, 'full' );

                                echo fx_get_image_tag( site_url() .'/wp-content/uploads/2020/11/three-arrows-vertical.png', 'img-responsive img-desktop-tablet-only', false, 'full' );
                            ?>

                              <p>ATTAC Consulting Group</p>

                            </h2>
                        </div>
                    <?php endif; ?>
                    <div class="pos-relative innerpage-banner-text-container no-title">
                    <?php
                        if( !get_field('show_page_title') ) { // IF user/admin disabled to show page title
                            echo fx_get_image_tag( site_url() .'/wp-content/uploads/2020/11/three-arrows-horizontal.png','img-responsive img-mobile-only', false, 'full' );

                            echo fx_get_image_tag( site_url() .'/wp-content/uploads/2020/11/three-arrows-vertical.png', 'img-responsive img-desktop-tablet-only', false, 'full' );
                        }
                    ?>

                    <?php
                        // Get content and sanitize
                        $content = apply_filters('the_content', get_field('wysiwyg') );
                        $content = str_replace(']]>', ']]&gt;', $content);

                        echo $content;
                    ?>
                    </div>

                    <?php

                    if( get_field('include_a_button_on_banner', get_the_ID() ) ) : // IF user/admin enabled banner button
                        if( get_field('banner_button_text', get_the_ID() ) && get_field('banner_button_link', get_the_ID() ) ) : // Display button only if label and link are provided ?>

                    <div class="innerpage-banner-button-container">
                        <a class="btn" href="<?php echo get_field('banner_button_link', get_the_ID() ); ?>"><?php echo get_field('banner_button_text', get_the_ID() ); ?></a>
                    </div>

                    <?php endif;
                    endif; ?>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- Introduction Text Image Section -->
