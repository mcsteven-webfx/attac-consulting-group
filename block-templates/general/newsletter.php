<!-- Newsletter Section -->
<section class="page-content pos-relative" id="signup-newsletter">
    <!--  Hide Slanted Lines Left Side
    <?php // echo fx_get_image_tag( site_url() .'/wp-content/uploads/2020/11/slanted-lines-left-side.png', 'slant-lines left', false, 'full' ); ?>
    -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php echo fx_get_image_tag( site_url() . '/wp-content/uploads/2020/11/three-arrows-horizontal.png', 'img-responsive horizontal-arrows', false, 'full' ); ?>

                <?php if( get_field('subheading') ) : // IF subheading is provided ?>
                    <p class="service-subheading"><?php echo get_field('subheading'); ?></p>
                <?php else : ?>
                    <p class="service-subheading"><?php echo get_field('newsletter_subheading', 'option'); ?></p>
                <?php endif; ?>

                <?php if( get_field('heading') ) : // IF heading is provided ?>
                    <h2 class="h1"><?php echo get_field('heading'); ?></h2>
                <?php else : ?>
                    <?php if( get_sub_field('section_heading') ) : // IF section subheading is provided ?>
                        <h2 class="h1"><?php echo get_sub_field('section_heading'); ?></h2>
                    <?php else : ?>
                        <h2 class="h1"><?php echo get_field('newsletter_heading', 'option'); ?></h2>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
        <?php echo do_shortcode( get_field('newsletter_form', 'option') ); // Default newsletter form to use ?>
    </div>

    <!--  Hide Slanted Lines Right Side
    <?php // echo fx_get_image_tag( site_url() .'/wp-content/uploads/2020/11/slanted-lines-right-side.png', 'slant-lines right', false, 'full' ); ?>
    -->
</section>
<!-- Newsletter Section -->
