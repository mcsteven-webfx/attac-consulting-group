<!-- Homepage Banner -->
<section class="page-content pos-relative half-and-half flex-row" id="home-banner">
    <?php if( get_field('banner_background') ){
        echo fx_get_image_tag( get_field('banner_background')['url'], 'image-background hidden-sm-up', false, 'full' );
    } ?>
    <div class="half-and-half-text left">

        <div class="home-banner-text-container">
            <a href="<?php echo site_url() ?>"><?php echo fx_get_image_tag( site_url() .'/wp-content/uploads/2020/11/attac-logo-white-header-transparent-optimized.png', 'img-responsive mobile-logo img-mobile-only', false, 'full' ) ?></a>
            <p class="subheading"><?php echo get_field('banner_subheading'); ?></p>
            <h1 class="h1"><?php echo get_field('banner_heading'); ?></h1>
            <h6><?php the_field('bottom_heading'); ?></h6>
        </div>
    </div>
    <div class="half-and-half-image hidden-xs-down">
        <?php if( get_field('banner_background') ){
            echo fx_get_image_tag( get_field('banner_background')['url'], 'image-background', false, 'full' );
        } ?>
    </div>
</section>
<!-- Homepage Banner End -->
