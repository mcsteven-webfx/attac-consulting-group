<!-- Homepage Testimonial -->
<?php
// if user/admin choose to filter display by choosen category
if( get_field('category') ){
    $args = array(
        'post_type' => 'testimonial',
        'post_status'   => 'publish',
        'posts_per_page' => -1,
        'meta_key'       => 'featured',
        'meta_value'     => '1',
        'tax_query' => array(
            array(
                'taxonomy' => 'team_category',
                'field'    => 'term_id',
                'terms'    => get_field('category'),
            ),
        ),
    );
}else{ // if no filter was selected
    $args = array(
        'post_type' => 'testimonial',
        'post_status'   => 'publish',
        'posts_per_page' => -1,
        'meta_key'       => 'featured',
        'meta_value'     => '1',
    );
}

$theQuery = new WP_Query( $args );

// if there are testimonial to display
if( $theQuery->have_posts() ) : ?>
<!--  TESTIMONIAL  -->
<section class="page-content pos-relative" id="testimonial-section">
    <?php if( get_field('background') ){
        echo fx_get_image_tag( get_field('background')['url'], 'image-background', false, 'full' );
    } ?>
    <div class="container">
        <div class="row">
            <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-12">
                <p class="testimonial-quote">“</p>
                <h2 class="h1 testimonial-section-heading">Client Testimonials</h2>
                <div class="testimonial-slick-container slick-slider" data-slick-slidesToShow="1" data-slick-autoplay="true">
                    <?php
                    while( $theQuery->have_posts() ) : $theQuery->the_post(); ?>

                    <div class="testimonial-slick-item">
                        <div class="testimonial-content">
                            <a href="<?php echo get_the_permalink(); ?>">
                                <?php echo get_field('testimonial_content', get_the_ID() ); ?>
                            </a>
                        </div>
                        <div class="testimonial-name-and-location">
                            <a href="<?php echo get_the_permalink(); ?>">
                                <h4><?php echo get_field('client_name', get_the_ID() ); ?></h4>
                                <?php if( get_field('location', get_the_ID() ) ) : ?>
                                <p><?php echo get_field('location', get_the_ID() ); ?></p>
                                <?php endif; ?>
                            </a>
                        </div>
                    </div>

                    <?php
                    endwhile;
                    wp_reset_postdata();
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<!-- Homepage Testimonial -->
