<!-- Homepage About -->
<?php if( get_field('heading') && get_field('wysiwyg') ) : ?>
    <section class="page-content" id="about-section">
        <div class="container">
            <div class="row">
                <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-12">
                    <?php echo fx_get_image_tag( site_url() . '/wp-content/uploads/2020/11/three-arrows-horizontal.png', 'img-responsive horizontal-arrows', false, 'full' ); ?>
                    <p class="service-subheading"><?php echo get_field('subheading'); ?></p>
                    <h2 class="h1"><?php echo get_field('heading'); ?></h2>
                    <?php echo get_field('wysiwyg'); ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
<!-- Homepage About -->
