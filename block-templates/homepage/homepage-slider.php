<!-- Homepage Slider -->
<section class="page-content help-you-solve" id="">
    <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-12">
        <div class="new-slider">
            <div class="new-slider_slide">
                <!-- Hide background image - using solid color -->
                <!-- <img src="https://attacconsultinggroup.webpagefxstage.com/wp-content/uploads/2020/11/client-testimonial-background.jpg" alt=""> -->
                <div class="new-slider_content">
                    <div class="new-slider_content-wrapper">
                        <?php while( have_rows('slider') ) : the_row();?>
                        <div class="new-slider_content-inner-wrapper">
                            <h2><?php echo get_sub_field('heading'); ?></h2>
                            <p><?php echo get_sub_field('content'); ?></p>
                            <?php
                            $link = get_sub_field('button');
                            if( $link ):
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                                ?>
                                <a class='btn' href='<?php echo esc_url( $link_url ); ?>' target='<?php echo esc_attr( $link_target ); ?>'><?php echo esc_html( $link_title ); ?></a>
                            <?php endif; ?>
                        </div>
                        <?php endwhile;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Homepage Slider -->
