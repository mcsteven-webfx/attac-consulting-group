<!-- Homepage Services -->
<section class="page-content help-you-solve" id="help-you-solve">
    <div class="container">
        <div class="row">
            <div class="col-xxs-12 col-xs-12 col-sm-12 col-md-12">
                <h2 class="h1 flush-top"><?php echo get_field('heading'); ?></h2>
                <div class="service-section-description"><?php echo get_field('wysiwyg'); ?></div>

                <?php if( have_rows('cards') ): ?>
                    <div class="new-services__circles">
                        <?php while( have_rows('cards') ) : the_row();?>
                        <a href="<?php echo get_sub_field('link'); ?>" class="new-services__circle">
                            <div class="rounded-circle">
                                <img src="<?php echo get_sub_field('icon'); ?>" alt="">
                            </div>
                            <div class="circle-body text-center">
                                 <p><?php echo get_sub_field('name'); ?></p>
                            </div>
                        </a>
                        <?php endwhile;?>
                    </div>
                <?php endif; ?>

                <!-- Hidden on CSS -->
                <!--
                <div class="feat-services-container row">
                    <?php
                        /*$args = array(
                            'post_type' => 'any',
                            'post_status'   => 'publish',
                            'posts_per_page' => 4,
                            'orderby' => 'title',
                            'order'     => 'DESC',
                            'post__in'  => get_field('featured_services')
                        );

                        $services = new WP_Query( $args );

                        while( $services->have_posts() ) : $services->the_post(); ?>

                        <div class="col-xxs-12 col-xs-12 col-sm-3 col-md-3">
                            <div class="service-data-container pos-relative">
                                <?php echo fx_get_image_tag( get_the_post_thumbnail_url(),'img-responsive', false, 'full' ); ?>
                                <div class="service-hovered">
                                    <h3><?php echo get_the_title(); ?></h3>
                                    <?php
                                        $content = apply_filters('the_content', wp_trim_words( get_the_content(), 15 ) );
                                        $content = str_replace(']]>', ']]&gt;', $content);

                                        echo $content;
                                    ?>
                                    <a class="btn-tertiary learn-more" href="<?php echo get_the_permalink(); ?>"><span>Learn More</span> <span class="icon-Arrow"></span></a>
                                </div>
                            </div>
                        </div>

                        <?php
                        endwhile;
                        wp_reset_postdata();*/
                        ?>
                </div>
                -->
            </div>
        </div>
    </div>
</section>
<!-- Homepage Services -->
