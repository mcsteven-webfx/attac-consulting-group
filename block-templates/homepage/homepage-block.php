<?php
/**
 * $template note:
 *
 * Block names should be prefixed with acf/. So if the name you specified in
 * fx_register_block is 'your-block-name', the name you should use here is
 * 'acf/your-block-name'
 */
$template = [
	['acf/homepage-banner'],
	['acf/homepage-icontext'],
	['acf/homepage-slider'],
	['acf/homepage-testimonial'],
	['acf/newsletter'],
	['acf/homepage-sponsors'],
	['acf/homepage-about'],
];
?>
<InnerBlocks template="<?php echo esc_attr( wp_json_encode( $template ) )?>" templateLock="all" />
