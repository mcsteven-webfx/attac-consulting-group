<!-- Homepage Events and Sponsors -->
<?php if( get_field('logos') ) : ?>
    <section class="page-content" id="events-sponsors"
             <?php if( get_field('background_color') ) : ?>
                style="background-color: <?php echo get_field('background_color'); ?>;"
             <?php endif; ?>
             >
        <div class="container">
            <div class="row">
                <div class="col-xxs-12 col-xs-12 col-sm-4 col-md-3">
                    <div class="section-heading-with-arrow pos-relative">
                        <?php echo fx_get_image_tag( site_url() .'/wp-content/uploads/2020/11/three-arrows-vertical.png','img-responsive', false, 'full' ); ?>
                        <h2 class="h1"><?php echo get_field('heading'); ?></h2>
                    </div>
                </div>
                <div class="col-xxs-12 col-xs-12 col-sm-8 col-md-9">
                    <div class="events-sponsors-logo-container slick-slider three" data-slick-slidesToShow="3" data-slick-autoplay="true">
                        <?php
                        $logos = get_field('logos');

                        foreach( $logos as $logo ) : ?>
                            <div class="events-sponsors-logo-item">
                                <?php if( get_field('external_link', $logo['ID']) ) : ?>
                                    <a href="<?php echo get_field('external_link', $logo['ID']); ?>"><?php echo fx_get_image_tag( $logo['url'],'img-responsive', false, 'full' ); ?></a>
                                <?php else : ?>
                                    <?php echo fx_get_image_tag( $logo['url'],'img-responsive', false, 'full' ); ?>
                                <?php endif; ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
<!-- Homepage Events and Sponsors -->
