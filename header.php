<?php
    function isMobile() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }

    $detect = new Mobile_Detect;
?>

<!DOCTYPE html>
<html class="no-js" lang="zxx"> <!-- <?php language_attributes(); ?> -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="dns-prefetch" href="//fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
        <?php
        // Insert google fonts tag after this
        // please use the &display=swap url, and add font-display: swap; to your font-face css
        ?>

        <!--  OSWALD FONT  -->
        <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@200;300;400;500;600;700&display=swap" rel="stylesheet">

        <!--  MONTSERRAT FONT  -->
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>

        <?php do_action( 'fxtheme_before_header' ); ?>


        <!--  DESKTOP HEADER -->
        <header class="page-header page-header--fixed">
            <div class="container hidden-md-up mobile-top-bar soft-half-ends">


                        <!-- Check if mobile text and mobile link is not empty in Theme Settings -->
                        <?php if( get_field('mobile_cta_text', 'option') && get_field('mobile_cta_link', 'option') ) : ?>
                        <div class="mobile-top-bar">
                            <span class="mobile-cta-text"><?php echo get_field('mobile_cta_text', 'option'); ?>: </span>
                            <?php $phone = preg_replace( '/[^A-Za-z0-9\-]/', '', str_replace(' ', '', get_field('mobile_cta_link', 'option') ) ); ?>
                            <span class="mobile-cta-link"><a href="tel:<?php echo $phone; ?>"><?php echo get_field('mobile_cta_link', 'option'); ?></a></span>
                        </div>
                        <?php endif; ?>
                        <div class="tablet-contact-us-btn-wrapper  hidden-xs-down">
                            <a class="tablet-contact-us-btn" href="<?php echo get_field('cta_link', 'option'); ?>"><?php echo get_field('cta_text', 'option'); ?></a>
                        </div>

            </div>
            <div class="container hidden-xs-down">

                <div class="page-header-wrapper">
                    <div class="page-header-logo-wrapper">
                        <!-- MAIN LOGO -->
                        <a class="page-header-link pos-relative" href="<?php echo site_url(); ?>">
                            <?php echo fx_get_image_tag( get_field('main_logo', 'option'), 'page-header-logo', false, 'full', array('alt' => 'ATTAC Consulting Group') ); ?>
                            <?php echo fx_get_image_tag( get_field('scrolled_logo', 'option'), 'page-header-logo scrolled', false, 'full', array('alt' => 'ATTAC Consulting Group') ); ?>
                        </a>
                    </div>
                    <div class="page-header-menu-wrapper">
                        <!-- MAIN MENU -->
                        <?php ubermenu( 'main' , array( 'menu' => 2 ) ); ?>
                    </div>


                    <div class="page-header-search-cta-wrapper">

                            <!-- Search Button -->
                            <div class="page-header-search-wrapper">
                                <a class="header-search-btn-trigger"><span class="icon-Search"></span> <span>Search</span></a>
                            </div>

                            <!-- CTA Button -->
                            <div class="page-header-cta-btn-wrapper hidden-sm-down">
                                <a class="btn" href="<?php echo get_field('cta_link', 'option'); ?>"><?php echo get_field('cta_text', 'option'); ?></a>
                            </div>

                    </div>
                </div>

            </div>


            <!-- Search Form dropdown -->
            <div class="header-search-form-container hidden-xs-down">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <?php echo get_search_form(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </header>
