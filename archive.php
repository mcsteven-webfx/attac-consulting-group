<?php get_template_part( 'partials/inner-page-title' ); ?> 

<section class="page-content blog-listing">
    <div class="container">
        <div class="blog-listing-container row">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <?php get_template_part( 'partials/loop-content' ); ?>
            <?php endwhile; endif; ?>
        </div>
    </div>
</section>

<?php get_template_part( 'partials/pagination' ); ?>